<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';

///Company Routes..

$route['new_distributor'] = 'Ael_panel/new_distributor';
$route['save_disributor'] = 'Ael_panel/save_disributor';
$route['view_distributor'] = 'Ael_panel/view_distributor';
$route['profile'] = 'Ael_panel/profile';
$route['inbox'] = 'Ael_panel/inbox';

$route['logout'] = 'Ael_panel/admin_logout';
$route['update_dist'] = 'Ael_panel/update_dist';
$route['update_profile'] = 'Ael_panel/update_profile';
$route['update_password'] = 'Ael_panel/update_password';


$route['edit_distributor/(:any)'] = 'Ael_panel/edit_distributor/$1';
$route['delete_distributor/(:any)'] = 'Ael_panel/delete_distributor/$1';
$route['edit_profile/(:any)'] = 'Ael_panel/edit_profile/$1';
$route['change_password/(:any)'] = 'Ael_panel/change_password/$1';






////Distributor Routes
$route['distributor'] = 'Home/distributor_login';
$route['new_company'] = 'Dist_panel/new_company';
$route['save_company'] = 'Dist_panel/save_company';
$route['view_company'] = 'Dist_panel/view_company';
$route['distributor_profile'] = 'Dist_panel/distributor_profile';
$route['new_users'] = 'Dist_panel/new_users';
$route['save_users'] = 'Dist_panel/save_users';
$route['view_users'] = 'Dist_panel/view_users';
$route['edit_users'] = 'Dist_panel/edit_users';
$route['update_users'] = 'Dist_panel/update_users';


$route['add_product'] = 'Dist_panel/add_product';
$route['save_product'] = 'Dist_panel/save_product';
$route['view_product'] = 'Dist_panel/view_product';

$route['add_category'] = 'Dist_panel/add_category';

$route['save_category'] = 'Dist_panel/save_category';
$route['view_category'] = 'Dist_panel/view_category';
$route['edit_category'] = 'Dist_panel/edit_category';
$route['update_category'] = 'Dist_panel/update_category';
  
  // Purchases Product 
$route['purchase_all'] = 'Product/purchase_all';


$route['purchase_product/(:any)'] = 'Product/purchase_product/$1';
$route['update_product/(:any)'] = 'Product/update_product/$1';


$route['add_expenses'] = 'Dist_panel/add_expenses';
$route['save_expenses'] = 'Dist_panel/save_expenses';
$route['view_expenses'] = 'Dist_panel/view_expenses';
$route['edit_expenses'] = 'Dist_panel/edit_expenses';
$route['update_expenses'] = 'Dist_panel/update_expenses';



$route['add_salary'] = 'Dist_panel/add_salary';
$route['save_salary'] = 'Dist_panel/save_salary';
$route['view_salary'] = 'Dist_panel/view_salary';
$route['edit_salary'] = 'Dist_panel/edit_salary';
$route['update_salary'] = 'Dist_panel/update_salary';


$route['new_emp'] = 'Dist_panel/new_emp';

$route['save_emp'] = 'Dist_panel/save_emp';

$route['view_emp'] = 'Dist_panel/view_emp';
$route['update_emp'] = 'Dist_panel/update_emp';
$route['edit_company/(:any)'] ='Dist_panel/edit_company/$1';
$route['update_company'] = 'Dist_panel/update_company';


$route['product_purchase'] = 'Product/product_purchase';
$route['supplier_quantity/(:any)'] = 'Product/supplier_quantity/$1';
$route['all_barcode/(:any)'] = 'Product/all_barcode/$1';

$route['new_payment'] = 'Dist_panel/new_payment';



// Bank
$route['add_bank'] = 'Dist_panel/add_bank';
$route['save_bank'] = 'Dist_panel/save_bank';
$route['view_bank'] = 'Dist_panel/view_bank';
$route['update_bank'] = 'Dist_panel/update_bank';
$route['add_transection'] = 'Dist_panel/add_transection';
$route['save_transection'] = 'Dist_panel/save_transection';
$route['view_transection'] = 'Dist_panel/view_transection';


//  
$route['password/(:any)'] ='Dist_panel/password/$1';

$route['change_password'] = 'Dist_panel/change_password';

//reports
$route['purchase_form'] = 'Reports/purchase_form';
$route['purchase_report'] = 'Reports/purchase_report';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
