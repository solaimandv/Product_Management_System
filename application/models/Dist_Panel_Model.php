<?php


class Dist_Panel_Model extends CI_Model {

   



 /////// Distributor Model Start

  public function save_comp_info() {
        $data = array();
        
$data['comp_name'] = $this->input->post('comp_name', true);
$data['comp_add'] = $this->input->post('comp_add');

if ($_FILES['comp_logo']['name'] != NULL) {
            $config['upload_path'] = './files/comp/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('comp_logo')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Dist_panel/new_company');
            } else {
                $fdata = $this->upload->data();
                $data['comp_logo'] = $config['upload_path'] . $fdata['file_name'];
            }
        }

$data['comp_color'] = $this->input->post('comp_color', true);
$data['comp_phone'] = $this->input->post('comp_phone');

$data['comp_email'] = $this->input->post('comp_email');

$dist_id = $this->session->userdata('dist_id');


  $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_company', $data);
        $sdata=array();
        $sdata['message']='Your New Company Successfully Created';
        $this->session->set_userdata($sdata);
    }



    public function select_all_view_comp($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_company');
        $this->db->where('dist_id',$dist_id);
        $this->db->order_by('comp_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


      public function select_comp_info_by_id($comp_id) {
        $this->db->select("*");
        $this->db->from('tbl_company');
        $this->db->where('comp_id', $comp_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function update_comp_info($data, $comp_id)
    {
        $this->db->where('comp_id', $comp_id);
        $this->db->update('tbl_company', $data);
    }

    //// delete company

     public function delete_comp_info($comp_id) {
        $this->db->where('comp_id', $comp_id);
        // unlink(base_url($dist_picture));
        $this->db->delete('tbl_company');
    }







    /////// Profile


      public function select_dist_profile($dist_id){
        $this->db->select("*");
        $this->db->from('tbl_distributor');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }




//////// USers Model


     public function save_user_info() {
        $data = array();
        
$data['user_name'] = $this->input->post('user_name', true);
$data['user_email'] = $this->input->post('user_email');

$data['user_password'] = $this->input->post('user_password', true);
$data['user_phone'] = $this->input->post('user_phone');

$data['user_address'] = $this->input->post('user_address');
$data['user_label'] = $this->input->post('user_label');



$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_user', $data);
        $sdata=array();
        $sdata['message']='Your New Users Successfully Created';
        $this->session->set_userdata($sdata);
    }

 public function select_all_view_users($dist_id){
        $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }
 public function user_info_by_id($user_id){
        // $this->db->select("*");
        $this->db->from('tbl_user');
        $this->db->where('user_id', $user_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

public function update_user_info($data,$user_id)
{
     $this->db->where('user_id', $user_id);
     $this->db->update('tbl_user', $data);
}



  public function delete_user_info($user_id)
  {
     $this->db->where('user_id', $user_id);
        // unlink(base_url($dist_picture));
        $this->db->delete('tbl_user');
  }


 public function select_all_Pcat($dist_id){
        $this->db->select("*");
        $this->db->from('tbl_category');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


public function select_all_Pcats(){
        $this->db->select("*");
        $this->db->from('tbl_category');
        //$this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }



public function select_all_supplier(){
        $this->db->select("*");
        $this->db->from('tbl_supplier');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }



    public function select_all_company($dist_id){
        $this->db->select("*");
        $this->db->from('tbl_company');
        $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }





public function save_product_info() {
  $data = array();
        
//$data['supplier_id'] = $this->input->post('supplier_id');
$data['cat_id'] = $this->input->post('cat_id');

$data['product_name'] = $this->input->post('product_name');
$data['product_details'] = $this->input->post('product_details');

$data['purchase_price'] = $this->input->post('purchase_price');
$data['sale_price'] = $this->input->post('sale_price');
$data['retails_price'] = $this->input->post('retails_price');
$data['tax_vat'] = $this->input->post('tax_vat');
//$data['product_qty'] = $this->input->post('product_qty');
$data['product_date'] = $this->input->post('product_date');
//$data['dist_id'] = $this->input->post('dist_id');




$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   

 $data['comp_id'] = $this->input->post('comp_id');   
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_product', $data);
        $sdata=array();
        $sdata['message']='Your New Product Successfully Created';
        $this->session->set_userdata($sdata);
    }





public function select_all_product($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_product');
        $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function select_product_by_id($product_id) {
        $this->db->select("*");
        $this->db->from('tbl_product');
        $this->db->where('product_id',$product_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

    public function delete_product_info($product_id)
{
    $this->db->where('product_id', $product_id);
       $this->db->delete('tbl_product');
}

/// Add Category

public function save_category_info() {
  $data = array();
        


$data['cat_name'] = $this->input->post('cat_name');
$data['cat_details'] = $this->input->post('cat_details');


//$data['dist_id'] = $this->input->post('dist_id');




$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   

 // $data['comp_id'] = $this->input->post('comp_id');   
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_category', $data);
        $sdata=array();
        $sdata['message']='Your New Category Successfully Created';
        $this->session->set_userdata($sdata);
    }



public function select_all_category($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_category');
        $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

    public function category_info_by_id($cat_id) {
      
        $this->db->from('tbl_category');
        $this->db->where('cat_id',$cat_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


    public function update_cat_info($data,$cat_id)
{
     $this->db->where('cat_id', $cat_id);
     $this->db->update('tbl_category', $data);
}

public function delete_category_info($cat_id)
{
    $this->db->where('cat_id', $cat_id);
       $this->db->delete('tbl_category');
}







    ////// Expense 



     public function save_expense_info() {
        $data = array();
        
$data['expense_type'] = $this->input->post('expense_type', true);
$data['expense_name'] = $this->input->post('expense_name');

$data['expense_note'] = $this->input->post('expense_note', true);
$data['expense_cost'] = $this->input->post('expense_cost');

$data['expense_date'] = $this->input->post('expense_date');

$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_expense', $data);
        $sdata=array();
        $sdata['message']='Your New Expense Successfully Created';
        $this->session->set_userdata($sdata);
    }

public function select_all_expence($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_expense');
        $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


    public function expence_info_by_id($expense_id)
{
        // $this->db->select("*");
        $this->db->from('tbl_expense');
        $this->db->where('expense_id',$expense_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
}


public function update_expense_info($data,$expense_id)
{
     $this->db->where('expense_id', $expense_id);
     $this->db->update('tbl_expense', $data);
}


public function delete_expense_info($expense_id)
{
       $this->db->where('expense_id', $expense_id);
       $this->db->delete('tbl_expense');
}



// Bank Model 

     public function save_bank_info() {
        $data = array();
        
$data['bank_name'] = $this->input->post('bank_name', true);
$data['bank_shortname'] = $this->input->post('bank_shortname');

$data['account_no'] = $this->input->post('account_no', true);
$data['blance'] = $this->input->post('blance');



$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_bank', $data);
        $sdata=array();
        $sdata['message']='Your New Bank Successfully Created';
        $this->session->set_userdata($sdata);
    }


public function select_all_bank($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_bank');
       $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


public function select_bank_by_id($bank_id) {
        $this->db->select("*");
        $this->db->from('tbl_bank');
       $this->db->where('bank_id',$bank_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

public function update_bank_info($data,$bank_id)
{
     //$blance = $amount;
     $this->db->where('bank_id', $bank_id);
     $this->db->update('tbl_bank', $data);
}


    public function update_bank_trans_info($data,$bank_id)
{
     //$blance = $amount;
     $this->db->where('bank_id', $bank_id);
     $this->db->update('tbl_bank', $data);
}



    public function select_all_bank_only_one($bank_id) {
        $this->db->select("*");
        $this->db->from('tbl_bank');
       $this->db->where('bank_id',$bank_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


    public function save_trans_info()
    {
               $data = array();
        
$data['bank_id'] = $this->input->post('bank_id', true);
$data['trans_type'] = $this->input->post('trans_type');

$data['amount'] = $this->input->post('amount', true);
$data['note'] = $this->input->post('note');
// $data['transection_date'] = $this->input->post('transection_date');


$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('bank_transection', $data);
        $sdata=array();
        $sdata['message']='Your New Transection Successfully Created';
        $this->session->set_userdata($sdata);

    }

public function select_all_transbank($dist_id)
{
        $this->db->select("*");

        $this->db->from('bank_transection');
        $this->db->join('tbl_bank', 'bank_transection.bank_id = tbl_bank.bank_id', 'left'); 
        $this->db->where('bank_transection.dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
}



// / Add salary

    public function select_all_employee($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_employee');
        $this->db->where('dist_id',$dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


public function save_salary_info() {
        $data = array();
        
$data['emp_id'] = $this->input->post('emp_id', true);
$data['salary_amount'] = $this->input->post('salary_amount');

$data['salary_date'] = $this->input->post('salary_date', true);


$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_salary', $data);
        $sdata=array();
        $sdata['message']='Your New Salary Successfully Created';
        $this->session->set_userdata($sdata);
    }

 public function select_salary_info($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_salary');
       
        $this->db->join('tbl_employee' ,'tbl_salary.emp_id = tbl_employee.emp_id','left');
        $this->db->where('tbl_salary.dist_id',$dist_id);
         
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

     public function salary_info_by_id($salary_id) {
       
       $this->db->from('tbl_salary');
       $this->db->where('salary_id',$salary_id);
       
         
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }

public function update_salary_info($data,$salary_id)
{
     $this->db->where('salary_id', $salary_id);
     $this->db->update('tbl_salary', $data);
}


public function delete_salary_info($salary_id)
{
       $this->db->where('salary_id', $salary_id);
       $this->db->delete('tbl_salary');
}



///// EMployee Model


  public function save_emp_info() {
        $data = array();
        
$data['emp_name'] = $this->input->post('emp_name', true);
$data['emp_phone'] = $this->input->post('emp_phone');


$data['emp_email'] = $this->input->post('emp_email', true);
$data['emp_address'] = $this->input->post('emp_address');

$data['emp_nid'] = $this->input->post('emp_nid');

$data['emp_degi'] = $this->input->post('emp_degi');

if ($_FILES['emp_picture']['name'] != NULL) {
            $config['upload_path'] = './files/emp/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('emp_picture')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Dist_panel/new_company');
            } else {
                $fdata = $this->upload->data();
                $data['emp_picture'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
$data['emp_salary'] = $this->input->post('emp_salary');


$dist_id = $this->session->userdata('dist_id');


  $data['dist_id'] = $dist_id;   
        
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_employee', $data);
        $sdata=array();
        $sdata['message']='Your New Employee Successfully Created';
        $this->session->set_userdata($sdata);
    }


     public function select_emp_info_all($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_employee');
       $this->db->where('dist_id',$dist_id);
       // $this->db->join('tbl_employee' ,'tbl_salary.dist_id = tbl_employee.dist_id','left');
         
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }


 public function emp_info_by_id($emp_id) {
       
       $this->db->from('tbl_employee');
       $this->db->where('emp_id',$emp_id);
       $query_result = $this->db->get();
       $result = $query_result->row();
        return $result;
    }


public function delete_emp_info($emp_id)
{
        $this->db->where('emp_id', $emp_id);
       $this->db->delete('tbl_employee');
}

public function update_empl_info($data,$emp_id)
{
     $this->db->where('emp_id', $emp_id);
     $this->db->update('tbl_employee', $data);
}





/////// Change password

 public function select_distributor_profile($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_distributor');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


  


  public function update_dist_password($data, $dist_id){
        $this->db->where('dist_id', $dist_id);
        $this->db->update('tbl_distributor', $data);
    }






}