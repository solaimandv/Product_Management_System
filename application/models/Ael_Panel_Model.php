<?php


class Ael_Panel_Model extends CI_Model {

    
 
  
   /////// Distributor Model Start

      public function save_dist_info() {
        $data = array();
        
$data['dist_name'] = $this->input->post('dist_name', true);
$data['dist_email'] = $this->input->post('dist_email', true);
$data['dist_password'] = md5($this->input->post('dist_password'));
$data['dist_phone'] = $this->input->post('dist_phone', true);
$data['dist_address'] = $this->input->post('dist_address');


         
   if ($_FILES['dist_picture']['name'] != NULL) {
            $config['upload_path'] = './files/dist/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('dist_picture')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Ael_panel/new_distributor');
            } else {
                $fdata = $this->upload->data();
                $data['dist_picture'] = $config['upload_path'] . $fdata['file_name'];
            }
        }   
        
       $data['zone'] = $this->input->post('zone');
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_distributor', $data);
        $sdata=array();
        $sdata['message']='Your New Distributor Successfully Created';
        $this->session->set_userdata($sdata);
    }



    public function select_all_view_dist() {
        $this->db->select("*");
        $this->db->from('tbl_distributor');
        $this->db->order_by('dist_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }



      public function select_dist_info_by_id($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_distributor');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }



     public function update_dist_info($data, $dist_id){
        $this->db->where('dist_id', $dist_id);
        $this->db->update('tbl_distributor', $data);
    }



    public function delete_dist_info($dist_id,$dist_picture) {
        $this->db->where('dist_id', $dist_id);
         unlink(base_url($dist_picture));
        $this->db->delete('tbl_distributor');
    }



    ////// End Of Model Distributor Essential Part

////// Admin Profile


    public function select_admin_profile($admin_id) {
        $this->db->select("*");
        $this->db->from('tbl_admin');
        $this->db->where('admin_id', $admin_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


         public function update_admin_info($data, $admin_id){
        $this->db->where('admin_id', $admin_id);
        $this->db->update('tbl_admin', $data);
    }


  public function update_admin_password($data, $admin_id){
        $this->db->where('admin_id', $admin_id);
        $this->db->update('tbl_admin', $data);
    }



     




}
