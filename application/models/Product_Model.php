<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Admin_Model
 *
 * @author Mahabub
 */
class Product_Model extends CI_Model{



  public function check_stock_table($product_id) {
        $this->db->select("*");
        $this->db->from('tbl_stock');
        $this->db->where('product_id',$product_id);
       // $this->db->order_by('comp_id', 'desc');
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


     public function update_product_stocks_info($data,$product_id)
    {

    	 

 // echo '<pre>';
 //       print_r($data);
 //       exit();



        $this->db->where('product_id', $product_id);
        $this->db->update('tbl_stock', $data);
    }



public function insert_product_stocks_info($data)
{
        $this->db->insert('tbl_stock', $data);
       

}

/// Purchase Insert Product Reports



public function insert_product_purchase_innew() {
  $data = array();
        
//$data['supplier_id'] = $this->input->post('supplier_id');
$data['product_id'] = $this->input->post('product_id');

$data['invoice'] = $this->input->post('invoice');
$data['purchase_price'] = $this->input->post('purchase_price');
$data['total_price'] = $this->input->post('total_price');
$data['adv_price'] = $this->input->post('adv_price');
$data['due_price'] = $this->input->post('due_price');
$data['sales_price'] = $this->input->post('sale_price');
$data['retail_price'] = $this->input->post('retails_price');
$data['qty'] = $this->input->post('product_qty');


$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;   

 $data['comp_id'] = $this->input->post('comp_id');   
       
      
       // echo '<pre>';
       // print_r($data);
       // exit();


        $this->db->insert('tbl_purchase', $data);
        $sdata=array();
        $sdata['message']='Your New Product Purchase Successfull';
        $this->session->set_userdata($sdata);
    }



  public function select_product_purchases_comp($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_company');
        $this->db->where('dist_id', $dist_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }

 public function select_product_purchases_comp_qty($comp_id) {
        $this->db->select("*");
        $this->db->from('tbl_stock ');
         $this->db->join('tbl_company', 'tbl_company.comp_id=tbl_stock.comp_id', 'left');
        $this->db->where('tbl_stock.comp_id', $comp_id);
        $query_result = $this->db->get();
        $result = $query_result->result();
        return $result;
    }



 public function select_product_name_comp($comp_id) {
        $this->db->select("*");
        $this->db->from('tbl_company ');
         //$this->db->join('tbl_company', 'tbl_company.comp_id=tbl_stock.comp_id', 'left');
        $this->db->where('comp_id', $comp_id);
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }


     public function select_purchase_invoice($dist_id) {
        $this->db->select("*");
        $this->db->from('tbl_purchase p');
       
        $this->db->join('tbl_company c' ,'p.comp_id = c.comp_id','left');

        $this->db->join('tbl_product' ,'p.product_id = tbl_product.product_id');

        $this->db->join('tbl_distributor' ,'p.dist_id = tbl_distributor.dist_id');
      //  $this->db->join('tbl_company c' ,'p.comp_id = c.comp_id','left');
        $this->db->where('p.dist_id',$dist_id);

         //$this->db->limit('1');
         $this->db->order_by('purchase_id','desc');
        $query_result = $this->db->get();
        $result = $query_result->row();
        return $result;
    }










}