
<div class="col-xs-12">
<div class="main-content">
<div class="main-content-inner">
<div class="page-content">
<div class="row">
<!-- PAGE CONTENT BEGINS -->
<div class="space-6"></div>
<div id="printableArea">
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
<div class="widget-box transparent">
<div class="widget-header widget-header-large">
	<h3 class="widget-title grey lighter">
		<img src="<?php  
$this->load->model('Dist_Model');
$res = $this->Dist_Model->view_picture($dist_id);
echo base_url().$res->dist_picture;?>"  
style="height: 22px;width:22px;" >
		Purchase Invoice
	</h3>

	<div class="widget-toolbar no-border invoice-info">
		<span class="invoice-info-label">Invoice:</span>
		<span class="red"><?php echo $purchase_info->invoice?></span>

		<br />
		<span class="invoice-info-label">Date:</span>
		<span class="blue"><?php echo $purchase_info->purchase_date?></span>
	</div>

	<div class="widget-toolbar hidden-480">
		<a onclick="myFunction()">
			<i class="ace-icon fa fa-print"></i>
		</a>
	</div>
</div>

<div class="widget-body">
	<div class="widget-main padding-24">
		<div class="row">
			<div class="col-sm-6">
				<div class="row">
					<div class="col-xs-11 label label-lg label-info arrowed-in arrowed-right">
						<b>Company Info</b>
					</div>
				</div>

				<div>
					<ul class="list-unstyled spaced">
						<li>
							<i class="ace-icon fa fa-caret-right blue"></i><?php echo $purchase_info->comp_name?>
						</li>

						<li>
							<i class="ace-icon fa fa-caret-right blue"></i><?php echo $purchase_info->comp_email?>
						</li>

						<li>
							<i class="ace-icon fa fa-caret-right blue"></i>Phone:<b class="red"><?php echo $purchase_info->comp_phone?></b>
						</li>

						<li>
							<?php echo $purchase_info->comp_add?>
						</li>

						<li class="divider"></li>

						<li>
							<i class="ace-icon fa fa-caret-right blue"></i>
							Paymant Info
						</li>
					</ul>
				</div>
			</div><!-- /.col -->

			<div class="col-sm-6">
				<div class="row">
					<div class="col-xs-11 label label-lg label-success arrowed-in arrowed-right">
						<b>Distributor Info</b>
					</div>
				</div>

				<div>
					<ul class="list-unstyled  spaced">
						<li>
							<i class="ace-icon fa fa-caret-right green"></i><?php echo $purchase_info->dist_name?>
						</li>

						<li>
							<i class="ace-icon fa fa-caret-right green"></i><?php echo $purchase_info->dist_email?>
						</li>

						<li>
							<i class="ace-icon fa fa-caret-right green"></i><?php echo $purchase_info->dist_phone?>
						</li>

						<li class="divider"></li>

						<li>
							<i class="ace-icon fa fa-caret-right green"></i>
							<?php echo $purchase_info->dist_address?>
						</li>
					</ul>
				</div>
			</div><!-- /.col -->
		</div><!-- /.row -->

		<div class="space"></div>

		<div>
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th class="center">#</th>
						<th>Product Name</th>
						<th class="hidden-xs">Product Details</th>
						<th class="hidden-480">Product Quantity</th>
						<th>Purchase Price</th>
					</tr>
				</thead>

				<tbody>
					<tr>
						<td class="center">1</td>

						<td>
							<?php echo $purchase_info->product_name?>
						</td>
						<td class="hidden-xs">
							<?php echo $purchase_info->product_details?>
						</td>
						<td class="hidden-480"> <?php echo $purchase_info->product_qty?> </td>
						<td><?php echo $purchase_info->purchase_price?></td>
					</tr>

					
					
				</tbody>
			</table>
		</div>

		<div class="hr hr8 hr-double hr-dotted"></div>

		<div class="row">
			<div class="col-sm-4 pull-right">
				<h4 class="pull-right">
					Total :
					<span class="red"><?php echo $purchase_info->total_price?> TK</span>
					<hr>
				</h4>

			</div>

	</div>
			
	

<div class="row">

			<div class="col-sm-4 pull-right">
				<h4 class="pull-right">
					Advance :
					<span class="red"><?php echo $purchase_info->adv_price?> TK</span>
					<hr>
				</h4>
			</div>
	</div>

	<div class="row">
			<div class="col-sm-4 pull-right">
				<h4 class="pull-right">
					Due :
					<span class="red"><?php echo $purchase_info->due_price?> TK</span>
				</h4>

			</div>
				</div>
			
		</div>

		<div class="space-6"></div>
		<div class="well">
			Thank you for choosing <b><?php echo $purchase_info->dist_name?></b> Company products.
We believe you will be satisfied by our services.
		</div>
	</div>
</div>
</div>
</div>
</div>
</div>
<!-- PAGE CONTENT ENDS -->
</div><!-- /.col -->
</div><!-- /.row -->
</div><!-- /.page-content -->
</div>
</div><!-- /.main-content -->