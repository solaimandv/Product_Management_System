<div class="main-content">
<div class="main-content-inner">
<div class="page-content">


<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue">Product Info Table</h3>

<div class="clearfix">
<div class="pull-right tableTools-container"></div>
</div>
<div class="table-header">
All Product
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->

<table id="dynamic-table" class="table table-striped table-bordered table-hover">
<thead>


<tr>

<th class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>
</th>

<th>Product Name</th>

<th>Company name</th>
<th>Purchase Price</th>
<th>Sale Price</th>
<th>Retails Price</th>




<th>Action</th>


</tr>

</thead>

  <tbody>       
<?php
    foreach ($product_info as $v_dist)
        {
  ?>


<tr>


<td class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>

</td>

<td> <?php echo $v_dist->product_name ?></td>

<!-- <td><?php echo $v_dist->product_details?></td> -->
<td><?php echo $v_dist->cat_id?></td>
<td><?php echo $v_dist->purchase_price?></td>

<td><?php echo $v_dist->sale_price?></td>
<td><?php echo $v_dist->retails_price?></td>
<!-- <td><?php echo $v_dist->product_qty?></td> -->



<!-- 
<td>
<span>
<img src="<?php echo base_url().$v_dist->product_pic ?>" class="img-responsive" style="height: 130px; width: 220px;"></span>
</td>
 -->


<td>

<a class="green" href="<?php echo base_url()?>purchase_product/<?php echo $v_dist->product_id?>" title="Purchase Product">
	<img src="<?php echo base_url()?>assets/purchase_icon.png">
</a>

</td>
</tr>



<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>

