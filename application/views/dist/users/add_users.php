<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Users
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Users Profile
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_users">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>save_users" enctype="multipart/form-data">
			

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> User Name </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="user_name" placeholder="Name" class="col-xs-10 col-sm-5" />
				</div>
            </div>



           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email</label>

				<div class="col-sm-9">
					<input type="email" id="form-field-1" name="user_email" placeholder="Email" class="col-xs-10 col-sm-5" />
				</div>
           </div>



            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password</label>

				<div class="col-sm-9">
					<input type="password" id="form-field-1" name="user_password" placeholder="Password" class="col-xs-10 col-sm-5" />
				</div>
            </div>


             <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="user_phone" placeholder="Phone" class="col-xs-10 col-sm-5" />
				</div>
             </div>




            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address</label>

				<div class="col-sm-6">
			
	
						<textarea id="editor1" name="user_address"></textarea>

				</div>
            </div>


           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-select-3"> Label</label>

				<div class="col-sm-3">
				<select name="user_label"  class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose Users...">
					<option value="1">Manager</option>
					<option value="2">Salesman</option>
				</select>
					<!-- <input type="text" id="form-field-1" name="user_label" placeholder="label" class="col-xs-10 col-sm-5" /> -->
				</div>

           </div>



            	



            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>



