<div class="main-content">
<div class="main-content-inner">
<div class="page-content">


<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue">Users Info Table</h3>

<div class="clearfix">
<div class="pull-right tableTools-container"></div>
</div>
<div class="table-header">
All Users
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->

<table id="dynamic-table" class="table table-striped table-bordered table-hover">
<thead>


<tr>

<th class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>
</th>

<th>Name</th>
<th>Email</th>
<th >Phone</th>

<th>
Address
</th>
<th>Label</th>

<th class="">Action</th>


</tr>

</thead>

  <tbody>       
<?php
    foreach ($user_info as $v_dist)
        {
  ?>


<tr>


<td class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>

</td>

<td> <?php echo $v_dist->user_name ?></td>

<td><?php echo $v_dist->user_email ?></td>
<td><?php echo $v_dist->user_phone ?></td>
<td><?php echo $v_dist->user_address ?></td>

<td><?php 

if ($v_dist->user_label == 1 ){
	?> <p style="color:red; font-weight: 700"><?php echo "Manager";?></p> <?php
}

else {
	?><p style="color:Green; font-weight: 700"><?php echo "SalesMan";?></p> <?php
}


 ?></td>





<td>
<div class="hidden-sm hidden-xs action-buttons">


<a class="green" href="<?php echo base_url()?>Dist_panel/edit_users/<?php echo $v_dist->user_id?>">
	<i class="ace-icon fa fa-pencil bigger-130"></i>
</a>

<a class="red" href="<?php echo base_url()?>Dist_panel/delete_users/<?php echo $v_dist->user_id?>" onclick="return ask_for_delete()";>
	<i class="ace-icon fa fa-trash-o bigger-130"></i>
</a>
</div>



<div class="hidden-md hidden-lg">
<div class="inline pos-rel">
	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
	</button>

	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
		

		<li>
			<a href="<?php echo base_url()?>edit_users/<?php echo $v_dist->user_id?>" class="tooltip-success" data-rel="tooltip" title="Edit">
				<span class="green">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
				</span>
			</a>
		</li>

		<li>
			<a href="<?php echo base_url()?>delete_users/<?php echo $v_dist->user_id?>" class="tooltip-error" onclick="return ask_for_delete()"; data-rel="tooltip" title="Delete">
				<span class="red">
					<i class="ace-icon fa fa-trash-o bigger-120"></i>
				</span>
			</a>
		</li>
	</ul>
</div>
</div>
</td>
</tr>



<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>

