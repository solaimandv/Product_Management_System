<div class="main-content">
<div class="main-content-inner">
<div class="page-content">


<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue">Employee Info Table</h3>

<div class="clearfix">
<div class="pull-right tableTools-container"></div>
</div>
<div class="table-header">
All Employee
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->

<table id="dynamic-table" class="table table-striped table-bordered table-hover">
<thead>


<tr>



<th>Name</th>
<th>Phone</th>
<th >Email</th>

<th>
Address
</th>
<th>Information</th>
<th>Picture</th>

<th>Action</th>


</tr>

</thead>

  <tbody>       
<?php
    foreach ($emp_info as $v_dist)
        {
  ?>


<tr>




<td> <?php echo $v_dist->emp_name ?></td>

<td><?php echo $v_dist->emp_phone ?></td>
<td><?php echo $v_dist->emp_email ?></td>
<td><?php echo $v_dist->emp_address ?></td>
<td><b>Designation: </b><?php echo $v_dist->emp_degi ?>
<br>
	<b>Salary: </b><?php echo $v_dist->emp_salary ?>
</td>



<td>
<span>
<img src="<?php echo base_url().$v_dist->emp_picture ?>" class="img-responsive" style="height: 130px; width: 220px;"></span>
</td>



<td>
<div class="hidden-sm hidden-xs action-buttons">
<!-- <a class="blue" href="#">
	<i class="ace-icon fa fa-search-plus bigger-130"></i>
</a> -->

<a class="green" href="<?php echo base_url()?>Dist_panel/edit_employee/<?php echo $v_dist->emp_id?>">
	<i class="ace-icon fa fa-pencil bigger-130"></i>
</a>

<a class="red" href="<?php echo base_url()?>Dist_panel/delete_employee/<?php echo $v_dist->emp_id?>" onclick="return ask_for_delete()";>
	<i class="ace-icon fa fa-trash-o bigger-130"></i>
</a>
</div>



<div class="hidden-md hidden-lg">
<div class="inline pos-rel">
	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
	</button>

	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
		

		<li>
			<a href="<?php echo base_url()?>Dist_panel/edit_employee/<?php echo $v_dist->emp_id?>" class="tooltip-success" data-rel="tooltip" title="Edit">
				<span class="green">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
				</span>
			</a>
		</li>

		<li>
			<a href="<?php echo base_url()?>delete_distributor/<?php echo $v_dist->emp_id?>" class="tooltip-error" onclick="return ask_for_delete()"; data-rel="tooltip" title="Delete">
				<span class="red">
					<i class="ace-icon fa fa-trash-o bigger-120"></i>
				</span>
			</a>
		</li>
	</ul>
</div>
</div>
</td>
</tr>



<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>

