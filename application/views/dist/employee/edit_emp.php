<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Employee
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Employee Profile
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_emp">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>update_emp" enctype="multipart/form-data">
			

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Employee Name </label>

				<div class="col-sm-9">

				<input type="hidden" id="form-field-1" name="emp_id" value="<?php echo $emp_info->emp_id?>" placeholder="Name" class="col-xs-10 col-sm-5" />

					<input type="text" id="form-field-1" name="emp_name" value="<?php echo $emp_info->emp_name?>" placeholder="Name" class="col-xs-10 col-sm-5" />


				</div>
            </div>

            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Phone</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="emp_phone" value="<?php echo $emp_info->emp_phone?>" placeholder="Phone/Mobile" class="col-xs-10 col-sm-5" />
				</div>
            </div>


            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Email</label>

				<div class="col-sm-9">
					<input type="eamil" id="form-field-1" name="emp_email" value="<?php echo $emp_info->emp_email?>"  placeholder="Email" class="col-xs-10 col-sm-5" />
				</div>
            </div>


            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address</label>

				<div class="col-sm-6">
			
				
						
						<textarea id="editor1" name="emp_address"><?php echo $emp_info->emp_address?></textarea>
					
			
				</div>
            </div>


             <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">NID</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="emp_nid" value="<?php echo $emp_info->emp_nid?>" placeholder="National Id" class="col-xs-10 col-sm-5" />
				</div>
            </div>


             <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Designation</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="emp_degi" value="<?php echo $emp_info->emp_degi?>" placeholder="Designation" class="col-xs-10 col-sm-5" />
				</div>
            </div>



           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Picture</label>

				<div class="col-sm-9">
					<input type="file" id="form-field-1" name="emp_picture" placeholder="email" class="col-xs-10 col-sm-5" />
				
				</div>
				<br>

				<img src="<?php echo base_url(). $emp_info->emp_picture?>" height="120" width="150">
            </div>


            	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Employee Salary</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="emp_salary" value="<?php echo $emp_info->emp_salary?>" placeholder="Salary" class="col-xs-10 col-sm-5" />
				</div>
            </div>



            	
            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
