<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Company
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Company Profile
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_company">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>update_company" enctype="multipart/form-data">
			

			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Company Name </label>

				<div class="col-sm-9">
					<input type="hidden" id="form-field-1" name="comp_id"  value="<?php echo $comp_info->comp_id?>" placeholder="Name" class="col-xs-10 col-sm-5" />

					<input type="text" id="form-field-1" name="comp_name"  value="<?php echo $comp_info->comp_name?>" placeholder="Name" class="col-xs-10 col-sm-5" />
				</div>
            </div>


            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Address</label>

				<div class="col-sm-6">
			
				
						
						<textarea id="editor1" name="comp_add"><?php echo $comp_info->comp_add?></textarea>
					
			
				</div>
            </div>


            	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Company Logo</label>

				<div class="col-sm-9">
					<input type="file" id="form-field-1" name="comp_logo" placeholder="email" class="col-xs-10 col-sm-5" />
				</div>

				<br>
				<img src="<?php echo base_url().$comp_info->comp_logo?>" class="img-responsive img-rounded"  alt="Cinque Terre" width="100" height="150">
            </div>


            	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1">Choose Color</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="comp_color" value="<?php echo $comp_info->comp_color?>" placeholder="Color" class="col-xs-10 col-sm-5" />
				</div>
            </div>



            	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Phone</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="comp_phone" placeholder="Phone" value="<?php echo $comp_info->comp_phone?>" class="col-xs-10 col-sm-5" />
				</div>
            </div>

	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="comp_email"  value="<?php echo $comp_info->comp_email?>" placeholder="Phone" class="col-xs-10 col-sm-5" />
				</div>
            </div>
	



            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
