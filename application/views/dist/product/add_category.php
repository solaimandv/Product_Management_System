<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Category
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Users Profile
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_category">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>save_category" enctype="multipart/form-data">
			  
<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Name </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="cat_name" placeholder="Category Name" class="col-xs-10 col-sm-5" />
				</div>
            </div>


           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category Details</label>

				<div class="col-sm-6">
			
	
						<textarea id="editor1" name="cat_details"></textarea>

				</div>
            </div>
          
             




          




            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>



