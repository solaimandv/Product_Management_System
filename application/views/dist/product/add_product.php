<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Product
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Users Profile
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_product">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>save_product" enctype="multipart/form-data">
			  

<!--
             <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-select-3"> Supplier</label>

				<!-- <div class="col-sm-3">
				<select name="supplier_id"  class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose Users...">
					<option >------------Select Supplier-------------</option>
					<?php foreach ($supplier_info as $supplier_info) {?>
					<option value="<?php echo $supplier_info->supplier_id?>"><?php echo $supplier_info->supplier_name?></option>
					<?php }?>
				</select>
					
				</div>

           </div> -->


            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-select-3"> Category</label>

				<div class="col-sm-3">
				<select name="cat_id"  class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose Users...">
					<option >----------Select Category-----------</option>
					<?php foreach($cat_info as $cat_info) {?>
					<option value="<?php echo $cat_info->cat_id?>"><?php echo $cat_info->cat_name?></option>
					<?php }?>
				</select>
					<!-- <input type="text" id="form-field-1" name="user_label" placeholder="label" class="col-xs-10 col-sm-5" /> -->
				</div>

           </div>



         
			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Name </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="product_name" placeholder="Product Name" class="col-xs-10 col-sm-5" />
				</div>
            </div>


           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Details</label>

				<div class="col-sm-6">
			
	
						<textarea id="editor1" name="product_details"></textarea>

				</div>
            </div>
          
             



            


         

           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Purchase Price</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="purchase_price" placeholder="Purchase Price" class="col-xs-10 col-sm-5" />
				</div>
            </div>
            
            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Sale Price</label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="sale_price" placeholder="Sale Price" class="col-xs-10 col-sm-5" />
				</div>
            </div>

            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Retails Price </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="retails_price" placeholder="Retail Price " class="col-xs-10 col-sm-5" />
				</div>
            </div>
            	
            	<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Vat-Tax </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="tax_vat" placeholder="Vat-Tax" class="col-xs-10 col-sm-5" />
				</div>
            </div>

        <!--     <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Product Quantity </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="product_qty" placeholder="Product Quantity" class="col-xs-10 col-sm-5" />
				</div>
            </div> -->

            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="id-date-picker-1"> Product Date </label>

				 <div class="col-sm-9">
                                        <input class="col-xs-10 col-sm-5 date-picker" id="id-date-picker-1" type="date"  name="rcv_date" data-date-format="dd-mm-yyyy" />
                                     
                                    </div>           </div>

              


            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-select-3"> Company</label>

				<div class="col-sm-3">
				<select name="comp_id"  class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose Users...">
					<option >-------Select Company------</option>
					<?php foreach($comp_info as $comp_info) {?>
					<option value="<?php echo $comp_info->comp_id?>"><?php echo $comp_info->comp_name?></option>
					<?php }?>
				</select>
					<!-- <input type="text" id="form-field-1" name="user_label" placeholder="label" class="col-xs-10 col-sm-5" /> -->
				</div>

           </div>




            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>



