<div class="main-content">
<div class="main-content-inner">
<div class="page-content">


<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue">Transection Info Table</h3>

<div class="clearfix">
<div class="pull-right tableTools-container"></div>
</div>
<div class="table-header">
All Transection
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->

<table id="dynamic-table" class="table table-striped table-bordered table-hover">
<thead>


<tr>

<th class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>
</th>

<th>Bank Name</th>
<th>Branch Name</th>
<th>Transection Type</th>
<th>Amount</th> 
<th>Note</th>




<th>Date</th>


</tr>

</thead>

  <tbody>       
<?php
    foreach ($trans_info as $trans_info)
        {
  ?>


<tr>


<td class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>

</td>

<td> <?php echo $trans_info->bank_name ?></td>

<td><?php echo $trans_info->branc_name ?></td>
<td><?php echo $trans_info->trans_type ?></td>
<td><?php echo $trans_info->amount ?></td>
<td><?php echo $trans_info->note ?></td>
<td><?php echo $trans_info->transection_date ?></td>






</tr>



<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>

