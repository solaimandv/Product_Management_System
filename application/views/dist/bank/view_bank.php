<div class="main-content">
<div class="main-content-inner">
<div class="page-content">


<div class="row">
<div class="col-xs-12">
<h3 class="header smaller lighter blue">Bank Info Table</h3>

<div class="clearfix">
<div class="pull-right tableTools-container"></div>
</div>
<div class="table-header">
All Bank Info
</div>

<!-- div.table-responsive -->

<!-- div.dataTables_borderWrap -->

<table id="dynamic-table" class="table table-striped table-bordered table-hover">
<thead>


<tr>

<th class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>
</th>

<th>Bank Name</th>
<th>Branch Name</th>
<th>Bank Short Name</th>
<th>Account No</th> 
<th>Current Balance</th>




<th>Action</th>


</tr>

</thead>

  <tbody>       
<?php
    foreach ($bank_info as $bank_info)
        {
  ?>


<tr>


<td class="center">
<label class="pos-rel">
<input type="checkbox" class="ace" />
<span class="lbl"></span>
</label>

</td>

<td> <?php echo $bank_info->bank_name ?></td>

<td><?php echo $bank_info->branc_name ?></td>
<td><?php echo $bank_info->bank_shortname ?></td>
<td><?php echo $bank_info->account_no ?></td>
<td><?php echo $bank_info->blance ?></td>






<td>
<div class="hidden-sm hidden-xs action-buttons">
<!-- <a class="blue" href="#">
	<i class="ace-icon fa fa-search-plus bigger-130"></i>
</a> -->

<a class="green" href="<?php echo base_url()?>Dist_panel/edit_bank/<?php echo $bank_info->bank_id?>">
	<i class="ace-icon fa fa-pencil bigger-130"></i>
</a>

<a class="red" href="<?php echo base_url()?>Dist_panel/delete_expense/<?php echo $bank_info->bank_id?>/<?php echo $bank_info->bank_id?>" onclick="return ask_for_delete()";>
	<i class="ace-icon fa fa-trash-o bigger-130"></i>
</a>
</div>



<div class="hidden-md hidden-lg">
<div class="inline pos-rel">
	<button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
		<i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
	</button>

	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
		

		<li>
			<a href="<?php echo base_url()?>edit_distributor/<?php echo $bank_info->bank_id?>" class="tooltip-success" data-rel="tooltip" title="Edit">
				<span class="green">
					<i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
				</span>
			</a>
		</li>

		<li>
			<a href="<?php echo base_url()?>delete_distributor/<?php echo $bank_info->bank_id?>" class="tooltip-error" onclick="return ask_for_delete()"; data-rel="tooltip" title="Delete">
				<span class="red">
					<i class="ace-icon fa fa-trash-o bigger-120"></i>
				</span>
			</a>
		</li>
	</ul>
</div>
</div>
</td>
</tr>



<?php } ?>
</tbody>

</table>
</div>
</div>
</div>

</div>
</div>

