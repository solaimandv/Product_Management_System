<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12">



<div class="page-header">
							<h1>
								Salary
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									Add New Salary 
								</small>
							</h1>



							 <h3 class="text-center" style="color:green">
                <?php
                $message = $this->session->userdata('message');
                if ($message) {
                    echo $message;
                    $this->session->unset_userdata('message');
                }
                $exception = $this->session->userdata('exception');
                if ($exception) {
                    echo $exception;
                    $this->session->unset_userdata('exception');
                }
                ?>
            </h3>


            <div style="float: right;">
         <a href="<?php echo base_url()?>view_salary">
            <button class="btn btn-success">View All</button>
</a>
</div>

</div><!-- /.page-header -->

<div class="row">
	<div class="col-xs-12">
		<!-- PAGE CONTENT BEGINS -->
		<form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>save_salary" enctype="multipart/form-data">
			
               
           <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-select-3"> Employee Name</label>

				<div class="col-sm-4">
				<select name="emp_id"  class="chosen-select form-control" id="form-field-select-3" data-placeholder="Choose Users...">
					<option >----------Select Employee-----------</option>
					<?php foreach($emp_info as $emp_info) {?>
					<option value="<?php echo $emp_info->emp_id?>"><?php echo $emp_info->emp_name." (".$emp_info->emp_salary.")"?></option>
					<?php }?>
				</select>
					<!-- <input type="text" id="form-field-1" name="user_label" placeholder="label" class="col-xs-10 col-sm-5" /> -->
				</div>

           </div>






			<div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Salary Amount </label>

				<div class="col-sm-9">
					<input type="text" id="form-field-1" name="salary_amount" placeholder="Salary Amount" class="col-xs-10 col-sm-5" />
				</div>
            </div>


           
            <div class="form-group">
				<label class="col-sm-3 control-label no-padding-right" for="id-date-picker-1"> Date </label>

				<div class="col-sm-9">
					<input class="col-xs-10 col-sm-5" type="date" id="" name="salary_date" placeholder="Date Of Expense"  />


				</div>






            </div>


            



            

	
            	



            	


            	

            <div class="clearfix form-actions">
				<div class="col-md-offset-3 col-md-9">
					<button class="btn btn-info" >
						<i class="ace-icon fa fa-check bigger-110"></i>
						Submit
					</button>

					&nbsp; &nbsp; &nbsp;
					<button class="btn" type="reset">
						<i class="ace-icon fa fa-undo bigger-110"></i>
						Reset
					</button>
				</div>
			</div>





</form>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
