
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta charset="utf-8" />
<title><?php echo $title ?></title>

<meta name="description" content="Common form elements and layouts" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

<!-- bootstrap & fontawesome -->
<link rel="stylesheet" href="assets/css/bootstrap.min.css" />
<link rel="stylesheet" href="assets/font-awesome/4.5.0/css/font-awesome.min.css" />

<!-- page specific plugin styles -->
<link rel="stylesheet" href="assets/css/jquery-ui.custom.min.css" />
<link rel="stylesheet" href="assets/css/chosen.min.css" />
<link rel="stylesheet" href="assets/css/bootstrap-datepicker3.min.css" />
<link rel="stylesheet" href="assets/css/bootstrap-timepicker.min.css" />
<link rel="stylesheet" href="assets/css/daterangepicker.min.css" />
<link rel="stylesheet" href="assets/css/bootstrap-datetimepicker.min.css" />
<link rel="stylesheet" href="assets/css/bootstrap-colorpicker.min.css" />

<!-- text fonts -->
<link rel="stylesheet" href="assets/css/fonts.googleapis.com.css" />

<!-- ace styles -->
<link rel="stylesheet" href="assets/css/ace.min.css" class="ace-main-stylesheet" id="main-ace-style" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="assets/css/ace-part2.min.css" class="ace-main-stylesheet" />
<![endif]-->
<link rel="stylesheet" href="assets/css/ace-skins.min.css" />
<link rel="stylesheet" href="assets/css/ace-rtl.min.css" />

<!--[if lte IE 9]>
<link rel="stylesheet" href="assets/css/ace-ie.min.css" />
<![endif]-->

<!-- inline styles related to this page -->

<!-- ace settings handler -->
<script src="assets/js/ace-extra.min.js"></script>

<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

<!--[if lte IE 8]>
<script src="assets/js/html5shiv.min.js"></script>
<script src="assets/js/respond.min.js"></script>
<![endif]-->
</head>

<body class="no-skin">










<div class="main-content">
<div class="main-content-inner">
<div class="page-content">




<div class="row">
<div class="col-xs-12"> 


        <!-- #section:basics/content.breadcrumbs -->
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="ace-icon fa fa-home home-icon"></i>
                    <a href="<?php echo base_url()?>Dist_panel">Home</a>
                </li>

                
            </ul>

            </div>

<div class="page-content">


<section >
<form id="sales" >
<div class="wrapper">
<div class="row">                                
    <div class="panel panel-default">
        <div class="col-md-8" style="border:0px solid #ff5722;">
            <div class="row">
                <div class="col-md-12"><h1 style="color:#4527a0;"><b><?php echo $dist_name ?>(</b><b id='count'>0</b><b>)</b></h1></div>
                <div class="col-md-12">
                    <table border="1" style="background-color:#99CCFF;border-collapse:collapse;border:1px solid #666699;color:#000000;width:100%" cellpadding="3" cellspacing="3">
                        <tr>
                            <td>Invoice No.</td>
                            <!-- <td>
                                Customer ID
                            </td> -->
                            <!-- <td>Phone</td> -->
                            <td>Payment type</td>
                        </tr>
                        <tr>
                            <td><input readonly type="text" name="invoice_id" class="Textbox" value="<?php echo $invoice; ?>" style="width: 50%;"/></td>

                     <!--        <td><input onchange="customerss()" name="customer_id" id="customer" type="text" class="Textbox" value="" /></td> -->
                         <!--    <td><input onchange="phones()"  id="phone" type="text" class="Textbox" value="" /></td> -->

                            <td>
                                <select id="paymenttpe" name="payment_type" onchange="paymenttpe2()" class="Textbox"style="width: 40%;">
                                    <option value="cash" >Cash</option>
                                    <option value="card" >Card</option>
                                </select>
                            </td>
                        </tr>
                    </table>

                </div>
            </div>
            <div class="row" style="margin-top:5px;">
                <div class="col-md-4">
                    <input id="addtocart" onchange="addtocart2()" type="text" class="Textbox" placeholder="Barcode/Product ID" value=""  />
                </div>
              <!--   <div class="col-md-4">
                    <input id="addtocart" onchange="addtocart2()" type="text" class="Textbox" placeholder="Product Name" value=""  />
                </div> -->
                
                <div class="col-md-4"><input type="button" onclick="addtocart2()" class="Textbox btn btn-success" value="Add Item" /></div>
            </div>
            <div class="row" >
                <div class="col-md-12">
                    <table border="0" style="width:100%;margin-top:10px;">
                        <tr>
                            <td width="40%"><b>Product Name</b></td>
                            <td width="10%"><b>Price</b></td>
                            <td width="10%"><b>Quantity</b></td>
                            <td width="10%"><b>VAT</b></td>
                            <td width="10%"><b>Dis</b></td>
                            <td width="10%"><b>Total</b></td>
                             <td width="5%"><b>Remove</b></td>

                        </tr>
                    </table>
                </div>
                <div class="col-md-12" style="padding:0px;border: 1px solid graytext; background: white none repeat scroll 0% 0%; height:290px; width: 96%; margin-left: 2%; margin-right: 2%; margin-top: 10px; overflow: scroll;" >
                    <!------------------------------------------------------------------>

                    <table id="cattable" border="1" style="background-color:#FFFFCC;border-collapse:collapse;border:1px solid #FFCC00;color:#000000;width:100%" cellpadding="3" cellspacing="3">


                    </table>


                    <!------------------------------------------------------------------>
                </div>
            </div>


        </div>                                  
        <div class="col-md-4" style="border:0px solid #ff5722;">

             <div class="col-md-12">
                            <h1 style="color:red;">
                                <b>TK. </b>
                                <b id="totoals"></b>
                                <input name="totalprices" id="totalprices" type="hidden" class="Textbox" value="" />
                            </h1>
                        </div>
            <!---------------------------------------------------------------------->


            <p>Payment Information</p>
                        <table border="1" style="background-color:FFFFCC;border-collapse:collapse;border:1px solid FFCC00;color:000000;width:100%" cellpadding="3" cellspacing="3">
                            <tr>
                                <td>Total VAT</td>
                                <td><input id="vat" name="vat" readonly  type="text" class="Textbox" value="0.00" /></td>
                            </tr>
                            <tr>
                                <td style="width:100px;">Cash Paid</td>
                                <td><input name="cash" required id="paid" onchange="duechange()" type="text" class="Textbox" value="0.00" /></td>
                            </tr>
                           
                            <tr>
                                <td>Due</td>
                                <td><input name="due" id="due"  readonly type="text" class="Textbox" value="0.00" /></td>
                            </tr>
                            <tr>
                                <td>Change</td>
                                <td><input  name="change" id="change" readonly type="text" class="Textbox" value="0.00" /></td>
                            </tr>
                           

                        </table>

            <p id="cardt" style="display:none;">Card Payment</p>
            <table id="card" border="1" style="display:none;background-color:FFFFCC;border-collapse:collapse;border:1px solid FFCC00;color:000000;width:100%" cellpadding="3" cellspacing="3">
                <tr>
                    <td>Card type</td>
                    <td>
                        <select name="cardtype" class="Textbox">
                            <option>VISA</option>
                            <option>MASTER</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Amount Charged</td>
                    <td><input name="card_amount" type="text" class="Textbox" value="" /></td>
                </tr>
                <tr>
                    <td>Card Number</td>
                    <td><input name="card_number" type="text" class="Textbox" value="" /></td>
                </tr>
                <!--
                <tr>
                    <td>Expiration Date</td>
                    <td><input name="card_exp" type="text" class="Textbox" value="" /></td>
                </tr>
                -->

            </table>

            <table border="0" style="background-color:FFFFCC;border-collapse:collapse;border:1px solid FFCC00;color:000000;width:100%" cellpadding="3" cellspacing="3">
                <tr>

                    <td>
                        <input type="button" onclick="submitsales()" class="sub" value="Submit" />

                        <style>
                            .sub{border:0px; width:100%;background-color: red; height: 50px; font-size: 34px; font-weight: bold; color: white; border-radius: 5px; margin-top: 10px;}
                        </style>
                    </td>
                </tr>


            </table>

            <!---------------------------------------------------------------------->
        </div>
    </div>
</div>

</div>
</form>
</section>

<div class="footer">
<div class="footer-inner">
<div class="footer-content">
<span class="bigger-120">
<span class="blue bolder">Distributor</span>
&copy; 2017
</span>

&nbsp; &nbsp;
<span class="action-buttons">
<a href="#">
<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
</a>

<a href="#">
<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
</a>

<a href="#">
<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
</a>
</span>
</div>
</div>
</div>

<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
</a>



</div>
</div><!--/span-->
</div>
</div>
</div>

</div>
</div>
</div>






<!-- basic scripts -->

<!--[if !IE]> -->
<script src="assets/js/jquery-2.1.4.min.js"></script>

<!-- <![endif]-->

<!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
<script type="text/javascript">
if('ontouchstart' in document.documentElement) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- page specific plugin scripts -->

<!--[if lte IE 8]>
<script src="assets/js/excanvas.min.js"></script>
<![endif]-->
<script src="assets/js/jquery-ui.custom.min.js"></script>
<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="assets/js/chosen.jquery.min.js"></script>
<script src="assets/js/spinbox.min.js"></script>
<script src="assets/js/bootstrap-datepicker.min.js"></script>
<script src="assets/js/bootstrap-timepicker.min.js"></script>
<script src="assets/js/moment.min.js"></script>
<script src="assets/js/daterangepicker.min.js"></script>
<script src="assets/js/bootstrap-datetimepicker.min.js"></script>
<script src="assets/js/bootstrap-colorpicker.min.js"></script>
<script src="assets/js/jquery.knob.min.js"></script>
<script src="assets/js/autosize.min.js"></script>
<script src="assets/js/jquery.inputlimiter.min.js"></script>
<script src="assets/js/jquery.maskedinput.min.js"></script>
<script src="assets/js/bootstrap-tag.min.js"></script>

<!-- ace scripts -->
<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

<script src="<?php echo base_url(); ?>asset/js/jquery.min.js"></script>
<!-- inline scripts related to this page -->
<script type="text/javascript">
jQuery(function($) {
    $('#id-disable-check').on('click', function() {
        var inp = $('#form-input-readonly').get(0);
        if(inp.hasAttribute('disabled')) {
            inp.setAttribute('readonly' , 'true');
            inp.removeAttribute('disabled');
            inp.value="This text field is readonly!";
        }
        else {
            inp.setAttribute('disabled' , 'disabled');
            inp.removeAttribute('readonly');
            inp.value="This text field is disabled!";
        }
    });


    if(!ace.vars['touch']) {
        $('.chosen-select').chosen({allow_single_deselect:true}); 
        //resize the chosen on window resize

        $(window)
        .off('resize.chosen')
        .on('resize.chosen', function() {
            $('.chosen-select').each(function() {
                 var $this = $(this);
                 $this.next().css({'width': $this.parent().width()});
            })
        }).trigger('resize.chosen');
        //resize chosen on sidebar collapse/expand
        $(document).on('settings.ace.chosen', function(e, event_name, event_val) {
            if(event_name != 'sidebar_collapsed') return;
            $('.chosen-select').each(function() {
                 var $this = $(this);
                 $this.next().css({'width': $this.parent().width()});
            })
        });


        $('#chosen-multiple-style .btn').on('click', function(e){
            var target = $(this).find('input[type=radio]');
            var which = parseInt(target.val());
            if(which == 2) $('#form-field-select-4').addClass('tag-input-style');
             else $('#form-field-select-4').removeClass('tag-input-style');
        });
    }


    $('[data-rel=tooltip]').tooltip({container:'body'});
    $('[data-rel=popover]').popover({container:'body'});

    autosize($('textarea[class*=autosize]'));
    
    $('textarea.limited').inputlimiter({
        remText: '%n character%s remaining...',
        limitText: 'max allowed : %n.'
    });

    $.mask.definitions['~']='[+-]';
    $('.input-mask-date').mask('99/99/9999');
    $('.input-mask-phone').mask('(999) 999-9999');
    $('.input-mask-eyescript').mask('~9.99 ~9.99 999');
    $(".input-mask-product").mask("a*-999-a999",{placeholder:" ",completed:function(){alert("You typed the following: "+this.val());}});



    $( "#input-size-slider" ).css('width','200px').slider({
        value:1,
        range: "min",
        min: 1,
        max: 8,
        step: 1,
        slide: function( event, ui ) {
            var sizing = ['', 'input-sm', 'input-lg', 'input-mini', 'input-small', 'input-medium', 'input-large', 'input-xlarge', 'input-xxlarge'];
            var val = parseInt(ui.value);
            $('#form-field-4').attr('class', sizing[val]).attr('placeholder', '.'+sizing[val]);
        }
    });

    $( "#input-span-slider" ).slider({
        value:1,
        range: "min",
        min: 1,
        max: 12,
        step: 1,
        slide: function( event, ui ) {
            var val = parseInt(ui.value);
            $('#form-field-5').attr('class', 'col-xs-'+val).val('.col-xs-'+val);
        }
    });


    
    //"jQuery UI Slider"
    //range slider tooltip example
    $( "#slider-range" ).css('height','200px').slider({
        orientation: "vertical",
        range: true,
        min: 0,
        max: 100,
        values: [ 17, 67 ],
        slide: function( event, ui ) {
            var val = ui.values[$(ui.handle).index()-1] + "";

            if( !ui.handle.firstChild ) {
                $("<div class='tooltip right in' style='display:none;left:16px;top:-6px;'><div class='tooltip-arrow'></div><div class='tooltip-inner'></div></div>")
                .prependTo(ui.handle);
            }
            $(ui.handle.firstChild).show().children().eq(1).text(val);
        }
    }).find('span.ui-slider-handle').on('blur', function(){
        $(this.firstChild).hide();
    });
    
    
    $( "#slider-range-max" ).slider({
        range: "max",
        min: 1,
        max: 10,
        value: 2
    });
    
    $( "#slider-eq > span" ).css({width:'90%', 'float':'left', margin:'15px'}).each(function() {
        // read initial values from markup and remove that
        var value = parseInt( $( this ).text(), 10 );
        $( this ).empty().slider({
            value: value,
            range: "min",
            animate: true
            
        });
    });
    
    $("#slider-eq > span.ui-slider-purple").slider('disable');//disable third item

    
    $('#id-input-file-1 , #id-input-file-2').ace_file_input({
        no_file:'No File ...',
        btn_choose:'Choose',
        btn_change:'Change',
        droppable:false,
        onchange:null,
        thumbnail:false //| true | large
        //whitelist:'gif|png|jpg|jpeg'
        //blacklist:'exe|php'
        //onchange:''
        //
    });
    //pre-show a file name, for example a previously selected file
    //$('#id-input-file-1').ace_file_input('show_file_list', ['myfile.txt'])


    $('#id-input-file-3').ace_file_input({
        style: 'well',
        btn_choose: 'Drop files here or click to choose',
        btn_change: null,
        no_icon: 'ace-icon fa fa-cloud-upload',
        droppable: true,
        thumbnail: 'small'//large | fit
        //,icon_remove:null//set null, to hide remove/reset button
        /**,before_change:function(files, dropped) {
            //Check an example below
            //or examples/file-upload.html
            return true;
        }*/
        /**,before_remove : function() {
            return true;
        }*/
        ,
        preview_error : function(filename, error_code) {
            //name of the file that failed
            //error_code values
            //1 = 'FILE_LOAD_FAILED',
            //2 = 'IMAGE_LOAD_FAILED',
            //3 = 'THUMBNAIL_FAILED'
            //alert(error_code);
        }

    }).on('change', function(){
        //console.log($(this).data('ace_input_files'));
        //console.log($(this).data('ace_input_method'));
    });
    
    
    //$('#id-input-file-3')
    //.ace_file_input('show_file_list', [
        //{type: 'image', name: 'name of image', path: 'http://path/to/image/for/preview'},
        //{type: 'file', name: 'hello.txt'}
    //]);

    
    

    //dynamically change allowed formats by changing allowExt && allowMime function
    $('#id-file-format').removeAttr('checked').on('change', function() {
        var whitelist_ext, whitelist_mime;
        var btn_choose
        var no_icon
        if(this.checked) {
            btn_choose = "Drop images here or click to choose";
            no_icon = "ace-icon fa fa-picture-o";

            whitelist_ext = ["jpeg", "jpg", "png", "gif" , "bmp"];
            whitelist_mime = ["image/jpg", "image/jpeg", "image/png", "image/gif", "image/bmp"];
        }
        else {
            btn_choose = "Drop files here or click to choose";
            no_icon = "ace-icon fa fa-cloud-upload";
            
            whitelist_ext = null;//all extensions are acceptable
            whitelist_mime = null;//all mimes are acceptable
        }
        var file_input = $('#id-input-file-3');
        file_input
        .ace_file_input('update_settings',
        {
            'btn_choose': btn_choose,
            'no_icon': no_icon,
            'allowExt': whitelist_ext,
            'allowMime': whitelist_mime
        })
        file_input.ace_file_input('reset_input');
        
        file_input
        .off('file.error.ace')
        .on('file.error.ace', function(e, info) {
            //console.log(info.file_count);//number of selected files
            //console.log(info.invalid_count);//number of invalid files
            //console.log(info.error_list);//a list of errors in the following format
            
            //info.error_count['ext']
            //info.error_count['mime']
            //info.error_count['size']
            
            //info.error_list['ext']  = [list of file names with invalid extension]
            //info.error_list['mime'] = [list of file names with invalid mimetype]
            //info.error_list['size'] = [list of file names with invalid size]
            
            
            /**
            if( !info.dropped ) {
                //perhapse reset file field if files have been selected, and there are invalid files among them
                //when files are dropped, only valid files will be added to our file array
                e.preventDefault();//it will rest input
            }
            */
            
            
            //if files have been selected (not dropped), you can choose to reset input
            //because browser keeps all selected files anyway and this cannot be changed
            //we can only reset file field to become empty again
            //on any case you still should check files with your server side script
            //because any arbitrary file can be uploaded by user and it's not safe to rely on browser-side measures
        });
        
        
        /**
        file_input
        .off('file.preview.ace')
        .on('file.preview.ace', function(e, info) {
            console.log(info.file.width);
            console.log(info.file.height);
            e.preventDefault();//to prevent preview
        });
        */
    
    });

    $('#spinner1').ace_spinner({value:0,min:0,max:200,step:10, btn_up_class:'btn-info' , btn_down_class:'btn-info'})
    .closest('.ace-spinner')
    .on('changed.fu.spinbox', function(){
        //console.log($('#spinner1').val())
    }); 
    $('#spinner2').ace_spinner({value:0,min:0,max:10000,step:100, touch_spinner: true, icon_up:'ace-icon fa fa-caret-up bigger-110', icon_down:'ace-icon fa fa-caret-down bigger-110'});
    $('#spinner3').ace_spinner({value:0,min:-100,max:100,step:10, on_sides: true, icon_up:'ace-icon fa fa-plus bigger-110', icon_down:'ace-icon fa fa-minus bigger-110', btn_up_class:'btn-success' , btn_down_class:'btn-danger'});
    $('#spinner4').ace_spinner({value:0,min:-100,max:100,step:10, on_sides: true, icon_up:'ace-icon fa fa-plus', icon_down:'ace-icon fa fa-minus', btn_up_class:'btn-purple' , btn_down_class:'btn-purple'});

    //$('#spinner1').ace_spinner('disable').ace_spinner('value', 11);
    //or
    //$('#spinner1').closest('.ace-spinner').spinner('disable').spinner('enable').spinner('value', 11);//disable, enable or change value
    //$('#spinner1').closest('.ace-spinner').spinner('value', 0);//reset to 0


    //datepicker plugin
    //link
    $('.date-picker').datepicker({
        autoclose: true,
        todayHighlight: true
    })
    //show datepicker when clicking on the icon
    .next().on(ace.click_event, function(){
        $(this).prev().focus();
    });

    //or change it into a date range picker
    $('.input-daterange').datepicker({autoclose:true});


    //to translate the daterange picker, please copy the "examples/daterange-fr.js" contents here before initialization
    $('input[name=date-range-picker]').daterangepicker({
        'applyClass' : 'btn-sm btn-success',
        'cancelClass' : 'btn-sm btn-default',
        locale: {
            applyLabel: 'Apply',
            cancelLabel: 'Cancel',
        }
    })
    .prev().on(ace.click_event, function(){
        $(this).next().focus();
    });


    $('#timepicker1').timepicker({
        minuteStep: 1,
        showSeconds: true,
        showMeridian: false,
        disableFocus: true,
        icons: {
            up: 'fa fa-chevron-up',
            down: 'fa fa-chevron-down'
        }
    }).on('focus', function() {
        $('#timepicker1').timepicker('showWidget');
    }).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
    
    

    
    if(!ace.vars['old_ie']) $('#date-timepicker1').datetimepicker({
     //format: 'MM/DD/YYYY h:mm:ss A',//use this option to display seconds
     icons: {
        time: 'fa fa-clock-o',
        date: 'fa fa-calendar',
        up: 'fa fa-chevron-up',
        down: 'fa fa-chevron-down',
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-arrows ',
        clear: 'fa fa-trash',
        close: 'fa fa-times'
     }
    }).next().on(ace.click_event, function(){
        $(this).prev().focus();
    });
    

    $('#colorpicker1').colorpicker();
    //$('.colorpicker').last().css('z-index', 2000);//if colorpicker is inside a modal, its z-index should be higher than modal'safe

    $('#simple-colorpicker-1').ace_colorpicker();
    //$('#simple-colorpicker-1').ace_colorpicker('pick', 2);//select 2nd color
    //$('#simple-colorpicker-1').ace_colorpicker('pick', '#fbe983');//select #fbe983 color
    //var picker = $('#simple-colorpicker-1').data('ace_colorpicker')
    //picker.pick('red', true);//insert the color if it doesn't exist


    $(".knob").knob();
    
    
    var tag_input = $('#form-field-tags');
    try{
        tag_input.tag(
          {
            placeholder:tag_input.attr('placeholder'),
            //enable typeahead by specifying the source array
            source: ace.vars['US_STATES'],//defined in ace.js >> ace.enable_search_ahead
            /**
            //or fetch data from database, fetch those that match "query"
            source: function(query, process) {
              $.ajax({url: 'remote_source.php?q='+encodeURIComponent(query)})
              .done(function(result_items){
                process(result_items);
              });
            }
            */
          }
        )

        //programmatically add/remove a tag
        var $tag_obj = $('#form-field-tags').data('tag');
        $tag_obj.add('Programmatically Added');
        
        var index = $tag_obj.inValues('some tag');
        $tag_obj.remove(index);
    }
    catch(e) {
        //display a textarea for old IE, because it doesn't support this plugin or another one I tried!
        tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
        //autosize($('#form-field-tags'));
    }
    
    
    /////////
    $('#modal-form input[type=file]').ace_file_input({
        style:'well',
        btn_choose:'Drop files here or click to choose',
        btn_change:null,
        no_icon:'ace-icon fa fa-cloud-upload',
        droppable:true,
        thumbnail:'large'
    })
    
    //chosen plugin inside a modal will have a zero width because the select element is originally hidden
    //and its width cannot be determined.
    //so we set the width after modal is show
    $('#modal-form').on('shown.bs.modal', function () {
        if(!ace.vars['touch']) {
            $(this).find('.chosen-container').each(function(){
                $(this).find('a:first-child').css('width' , '210px');
                $(this).find('.chosen-drop').css('width' , '210px');
                $(this).find('.chosen-search input').css('width' , '200px');
            });
        }
    })
    /**
    //or you can activate the chosen plugin after modal is shown
    //this way select element becomes visible with dimensions and chosen works as expected
    $('#modal-form').on('shown', function () {
        $(this).find('.modal-chosen').chosen();
    })
    */

    
    
    $(document).one('ajaxloadstart.page', function(e) {
        autosize.destroy('textarea[class*=autosize]')
        
        $('.limiterBox,.autosizejs').remove();
        $('.daterangepicker.dropdown-menu,.colorpicker.dropdown-menu,.bootstrap-datetimepicker-widget.dropdown-menu').remove();
    });

});
</script>



<script type="text/javascript">
$(".form_datetime").datetimepicker({format: 'yyyy-mm-dd hh:ii'});
</script> 





<script type="text/javascript">
function printDiv(printableArea) {
var printContents = document.getElementById(printableArea).innerHTML;
var originalContents = document.body.innerHTML;
document.body.innerHTML = printContents;
window.print();
document.body.innerHTML = originalContents;
}
</script>

<script type="text/javascript">
<?php
$session_data = $this->session->userdata('dist_id');
// $branch_id = $this->session->userdata('branch_id');
?>



$(document).keypress(function(event) {
var keycode = (event.keyCode ? event.keyCode : event.which);
if(keycode == '13') {
//addtocart2();    
}
});


var box = [''];


function addtocart2() {

var barcode = $('#addtocart').val();

//alert(customer);


if(barcode==''){ alert("Enter barcode number");}
else{
/// barcode check------start 
var qtyneed = prompt("Please enter Quantity","0");
//  alert('Loading');

if(qtyneed>0){
//quantity check-----------start    

var a = box.indexOf(barcode);
// alert(box);
if (a > -1) {
//document.getElementById("demo").innerHTML = a;

alert('Product Is already in List');
$('#addtocart').val('');
return false;
}
else {


$.ajax({
    type: "POST",
    url: "<?php echo base_url();?>sales/addtocart",
    data: {barcode: barcode, qtyneed:qtyneed}
})
        .done(function(msg) {
    //alert(msg);
    
    if (msg == 1) {
        alert("Barcode Not in List !");
    }
    if(msg == 2){
        alert("Quantity not available");
    }
    if(msg == 3){
        alert("Product Not in your Store");
    }
    else {
        box.push(barcode);
        $('#cattable').append(msg);
        sum();
        duechange();
        $('#addtocart').val('');
    }

    


});

}
//quentity check ----------------------end
}
//barcode check end
}

}



 function remove_product(barcode) {
        //alert(barcode);
        var code = $('#' + barcode).attr('class');
        console.log(box);
        var i = box.indexOf(code);
        console.log(i);
        if (i != -1) {
            box.splice(i, 1);
        }
        console.log(box);

        $('#' + barcode).remove();
        sum();
        duechange();
    }

    
    function duechange() {
        var paid = $('#paid').val();
        console.log(paid);
        var totoals = $('#totoals').text();
        console.log(totoals);
        if (parseFloat(paid) < parseFloat(totoals)) {
            var due = (parseFloat(totoals) - parseFloat(paid));
            $('#due').val(due.toFixed(2));
            $('#change').val(0);
        }
        else {
            var change = (parseFloat(paid) - parseFloat(totoals));
            $('#due').val(0);
            $('#change').val(change.toFixed(2));
        }

    }
    function sum() {
        var sum = 0;

        $("#totalprice").each(function() {
            var val = $.trim($(this).text());

            if (val) {
                val = parseFloat(val.replace(/^\$/, ""));
                sum += !isNaN(val) ? val : 0;
            }
        });
        console.log(sum);
        var discount = $('#discount').val();
        var disamt = discount; 
        $('#disamt').val(disamt);
        
        var v = sum * <?php echo VAT; ?>;
        v.toFixed(2)
        
        $('#vat').val(v.toFixed(2));
        sum = sum + (v);
        sum = sum - disamt;
        duechange();
        $('#totoals').html(sum.toFixed(2));
        $('#totalprices').val(sum.toFixed(2));
         $('#due').html(sum.toFixed(2));
    var counter = (box.length)-1;
    $('#count').html(counter);
    }



 function paymenttpe2() {
        var paymenttpe = $('#paymenttpe').val();
        if (paymenttpe == 'cash') {
            //$('#card').hide();
            $("#card").css("display", "none");
            $("#cardt").css("display", "none");
            // $('#id').show();
        }
        if (paymenttpe == 'card') {
            $("#card").css("display", "block");
            $("#cardt").css("display", "block");
        }

    }

    function submitsales() {
        var paid=$('#paid').val();
        if(paid=='0.00'){
            alert("Paid Amount not insert yet!");
        }
        else{
        var sales = $("#sales").serialize();

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>Sales/sales",
            data: {sales: sales}
        })
                .done(function(msg) {
            //alert(msg);
                 //  var url="<?php echo base_url(); ?>pos/welcome/invoice_print?invoice_id="+msg;
                  // window.open(url,'_blank');
          var url = "<?php echo base_url(); ?>Sales/?invoice_id=" + msg;
           window.open(url, "_self");

            alert("Done..");
            //location.reload();
        });

        }


    }


</script>



</body>
</html>