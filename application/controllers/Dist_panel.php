<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dist_panel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



	 public function __construct() {
        parent::__construct();

        $dist_id = $this->session->userdata('dist_id');
       // $picture = $this->session->userdata('dist_pic');
        if ($dist_id == NULL) {
            redirect('Home');
        }
    }



	public function index()
	{
		$data = array();
		$data['title'] = "Home | Distributor";
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
       //  $data['dist_pic'] = $this->session->userdata('dist_pic');

       // echo '<pre>';
       // print_r( $data['dist_pic']);
       // exit();


        $dist_id = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_view_comp($dist_id);
		$data['admin_master'] = $this->load->view('dist/page_content', $data, true);
		$this->load->view('dist/home',$data);
	}

	

	//// Company Controller


	 public function new_company() {
        $data = array();
        $data['title'] = 'Add Company';
         $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['admin_master'] = $this->load->view('dist/company/add_comp', $data, true);
        $this->load->view('dist/home', $data);
    }

    /// Save Dist Info
	

     public function save_company() {

        $this->Dist_Panel_Model->save_comp_info();
        redirect('Dist_panel/new_company');
    }


    //// View All Company

      public function view_company() {
        
        $data = array();
        $data['title'] = 'Company Data';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_view_comp($dist_id);
        $data['admin_master'] = $this->load->view('dist/company/view_comp', $data, true);
        $this->load->view('dist/home', $data);
    

    }


    public function edit_company($comp_id)
    {
        $data = array();
        $data['title'] = 'Edit Company';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_comp_info_by_id($comp_id);
        $data['admin_master'] = $this->load->view('dist/company/edit_comp', $data, true);
        $this->load->view('dist/home', $data);
    }

public function update_company()
{
    $data = array();
    $comp_id = $this->input->post('comp_id', true); 
    $data['comp_name'] = $this->input->post('comp_name', true);
    $data['comp_add'] = $this->input->post('comp_add');

    

   $data['comp_color'] = $this->input->post('comp_color', true);
   $data['comp_phone'] = $this->input->post('comp_phone');

   $data['comp_email'] = $this->input->post('comp_email');

   $dist_id = $this->session->userdata('dist_id');


  // $data['dist_id'] = $dist_id;  
  if ($_FILES['comp_logo']['name'] != NULL) {
            $config['upload_path'] = './files/comp/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('comp_logo')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Dist_panel/new_company');
            } else {
                $fdata = $this->upload->data();
                $data['comp_logo'] = $config['upload_path'] . $fdata['file_name'];
            }
        }

        $this->Dist_Panel_Model->update_comp_info($data, $comp_id);
        redirect('Dist_panel/view_company');  

}

public function delete_company($comp_id)
{
    $this->Dist_Panel_Model->delete_comp_info($comp_id);
        
        redirect('Dist_Panel/view_company');
}


    //// Distributor Profile Data

    public function distributor_profile() {
        $data = array();
        $data['title'] = 'Profile';
         $data['dist_id'] = $this->session->userdata('dist_id');
          $data['dist_name'] = $this->session->userdata('dist_name');
         $dist_id = $this->session->userdata('dist_id');
         $data['dist_info'] = $this->Dist_Panel_Model->select_dist_profile($dist_id);
        $data['admin_master'] = $this->load->view('dist/profile/dist_profile', $data, true);
        $this->load->view('dist/home', $data);
    }


//// Change Password And information



/////// End Prodile Distributor



    //// User For distributor Controller


     public function new_users() {
        $data = array();
        $data['title'] = 'Add Users';
         $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['admin_master'] = $this->load->view('dist/users/add_users', $data, true);
        $this->load->view('dist/home', $data);
    }

    /// Save users Info
    

     public function save_users() {

        $this->Dist_Panel_Model->save_user_info();
        redirect('new_users');
    }


    //// View All Users

      public function view_users() {
        
        $data = array();
        $data['title'] = 'Users Data';
        $data['dist_id'] = $this->session->userdata('dist_id');
         $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['user_info'] = $this->Dist_Panel_Model->select_all_view_users($dist_id);
        $data['admin_master'] = $this->load->view('dist/users/view_users', $data, true);
        $this->load->view('dist/home', $data);
    
    }





    /////Edit users

 public function edit_users($user_id) {
        $data = array();
        $data['title'] = 'Add Users';
         $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['user_info']=$this->Dist_Panel_Model->user_info_by_id($user_id);
        $data['admin_master'] = $this->load->view('dist/users/edit_users', $data, true);
        $this->load->view('dist/home', $data);
    }

public function update_users()
    {
           $data = array();
 $user_id = $this->input->post('user_id', true);  
$data['user_name'] = $this->input->post('user_name', true);
$data['user_email'] = $this->input->post('user_email');


$data['user_phone'] = $this->input->post('user_phone');

$data['user_address'] = $this->input->post('user_address');
$data['user_label'] = $this->input->post('user_label');




$dist_id =$this->session->userdata('dist_id');

   $this->Dist_Panel_Model->update_user_info($data,$user_id);
        redirect('Dist_panel/view_users');  

    }



    /// Delete Users

public function delete_users($user_id)
{
    $this->Dist_Panel_Model->delete_user_info($user_id);
        
        redirect('Dist_Panel/view_users');
}


    /////// End Of users Data



///Add product

      public function add_product() {
        $data = array();
        $data['title'] = 'Add Product';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['cat_info'] = $this->Dist_Panel_Model->select_all_Pcat($dist_id);
       // $data['supplier_info'] = $this->Dist_Panel_Model->select_all_supplier();
        $dist_id = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_company($dist_id);
        $data['admin_master'] = $this->load->view('dist/product/add_product', $data, true);
        $this->load->view('dist/home', $data);
    }

public function save_product()
{
      $this->Dist_Panel_Model->save_product_info();
        redirect('Dist_panel/add_product');
}


 public function view_product() {
        
        $data = array();
        $data['title'] = 'View Product';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['product_info'] = $this->Dist_Panel_Model->select_all_product($dist_id);
        $data['admin_master'] = $this->load->view('dist/product/view_product', $data, true);
        $this->load->view('dist/home', $data);
    
    }
public function delete_product($product_id)
{
     $this->Dist_Panel_Model->delete_product_info($product_id);
        
        redirect('Dist_Panel/view_product');
}


  public function edit_product($product_id) {
        $data = array();
        $data['title'] = 'Edit Product';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['cat_info'] = $this->Dist_Panel_Model->select_all_Pcats();
       // $data['supplier_info'] = $this->Dist_Panel_Model->select_all_supplier();
        $dist_id = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_company($dist_id);
        $data['product_info'] = $this->Dist_Panel_Model->select_product_by_id($product_id);
        $data['admin_master'] = $this->load->view('dist/product/edit_product', $data, true);
        $this->load->view('dist/home', $data);
    }



     

///  Add Category


     public function add_category() {
        $data = array();
        $data['title'] = 'Add Category';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        
        $data['admin_master'] = $this->load->view('dist/product/add_category', $data, true);
        $this->load->view('dist/home', $data);
    }

public function save_category()
{
      $this->Dist_Panel_Model->save_category_info();
        redirect('Dist_panel/add_category');
}


public function view_category() {
        
        $data = array();
        $data['title'] = 'View Category';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['category_info'] = $this->Dist_Panel_Model->select_all_category($dist_id);
        $data['admin_master'] = $this->load->view('dist/product/view_category', $data, true);
        $this->load->view('dist/home', $data);
    
    }

// public function delete_category($cat_id)
// {
//     $this->Dist_Panel_Model->delete_category_info($cat_id);
        
//         redirect('Dist_Panel/view_category');
// }



     public function edit_category($cat_id) {
        $data = array();
        $data['title'] = 'Edit Category';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
    
        $dist_id = $this->session->userdata('dist_id');
         $data['cat_info'] = $this->Dist_Panel_Model->category_info_by_id($cat_id);
        $data['admin_master'] = $this->load->view('dist/product/edit_category', $data, true);
        $this->load->view('dist/home', $data);
    }


public function update_category() {
  $data = array();
        

 $cat_id = $this->input->post('cat_id', true);  
$data['cat_name'] = $this->input->post('cat_name');
$data['cat_details'] = $this->input->post('cat_details');


//$data['dist_id'] = $this->input->post('dist_id');




$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id;  


   $this->Dist_Panel_Model->update_cat_info($data,$cat_id);
        redirect('Dist_panel/view_category');  




}


    //////// Expenses Controller

    ///  Add 


     public function add_expenses() {
        $data = array();
        $data['title'] = 'Add Expenses';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['admin_master'] = $this->load->view('dist/expence/add_expense', $data, true);
        $this->load->view('dist/home', $data);
    }

public function save_expenses()
{
      $this->Dist_Panel_Model->save_expense_info();
        redirect('Dist_panel/add_expenses');
}


public function view_expenses() {
        
        $data = array();
        $data['title'] = 'View Expenses';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['expence_info'] = $this->Dist_Panel_Model->select_all_expence($dist_id);
        $data['admin_master'] = $this->load->view('dist/expence/view_expense', $data, true);
        $this->load->view('dist/home', $data);
    
    }


     public function edit_expenses($expense_id) {
        $data = array();
        $data['title'] = 'Edit Expenses';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['expense_info']=$this->Dist_Panel_Model->expence_info_by_id($expense_id);
        $data['admin_master'] = $this->load->view('dist/expence/edit_expense', $data, true);
        $this->load->view('dist/home', $data);
    }

public function update_expenses()
    {
           $data = array();
 $expense_id = $this->input->post('expense_id', true);  
$data['expense_type'] = $this->input->post('expense_type', true);
$data['expense_name'] = $this->input->post('expense_name');


$data['expense_note'] = $this->input->post('expense_note');

$data['expense_cost'] = $this->input->post('expense_cost');





$dist_id =$this->session->userdata('dist_id');

   $this->Dist_Panel_Model->update_expense_info($data,$expense_id);
        redirect('Dist_panel/view_expenses');  

    }



public function delete_expense($expense_id)
{
        $this->Dist_Panel_Model->delete_expense_info($expense_id);
        
        redirect('Dist_Panel/view_expenses');
}

///// Add Salary


     public function add_salary() {
        $data = array();
        $data['title'] = 'Add Salary';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['emp_info'] = $this->Dist_Panel_Model->select_all_employee($dist_id);
       
        $data['admin_master'] = $this->load->view('dist/salary/add_salary', $data, true);
         $this->load->view('dist/home', $data);
    }

public function save_salary()
{
      $this->Dist_Panel_Model->save_salary_info();
        redirect('Dist_panel/add_salary');
}


public function view_salary() {
        
        $data = array();
        $data['title'] = 'View Salary';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['salary_info'] = $this->Dist_Panel_Model->select_salary_info($dist_id);

       //    echo '<pre>';
       // print_r($data);
       // exit();
        $data['admin_master'] = $this->load->view('dist/salary/view_salary', $data, true);
        $this->load->view('dist/home', $data);
    
    }

  public function edit_salary($salary_id) {
        $data = array();
        $data['title'] = 'Edit Salary';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['emp_info'] = $this->Dist_Panel_Model->select_all_employee($dist_id);
       
       $data['salary_info'] = $this->Dist_Panel_Model->salary_info_by_id($salary_id);
        $data['admin_master'] = $this->load->view('dist/salary/edit_salary', $data, true);
         $this->load->view('dist/home', $data);
    }

  
public function update_salary()
    {
           $data = array();
$salary_id = $this->input->post('salary_id', true); 
$data['emp_id'] = $this->input->post('emp_id', true);
$data['salary_amount'] = $this->input->post('salary_amount');

$dist_id = $this->session->userdata('dist_id');

 $data['dist_id'] = $dist_id; 





$dist_id =$this->session->userdata('dist_id');

   $this->Dist_Panel_Model->update_salary_info($data,$salary_id);
        redirect('Dist_panel/view_salary');  

    }



public function delete_salary($salary_id)
{
    $this->Dist_Panel_Model->delete_salary_info($salary_id);
        
        redirect('Dist_Panel/view_salary');
}



// Bank Controller


public function add_bank() {
        $data = array();
        $data['title'] = 'Add Bank';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['admin_master'] = $this->load->view('dist/bank/add_bank', $data, true);
        $this->load->view('dist/home', $data);
    }

public function save_bank()
{
        $this->Dist_Panel_Model->save_bank_info();
        redirect('Dist_panel/add_bank');
}


public function edit_bank($bank_id) {
        $data = array();
        $data['title'] = 'Edit Bank';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
         $data['bank_info']=$this->Dist_Panel_Model->select_bank_by_id($bank_id);
        $data['admin_master'] = $this->load->view('dist/bank/edit_bank', $data, true);
        $this->load->view('dist/home', $data);
    }

public function update_bank()
{
        $data = array();
        $bank_id = $this->input->post('bank_id', true);  
        $data['bank_name'] = $this->input->post('bank_name', true);
         $data['branc_name'] = $this->input->post('branc_name', true);
        $data['bank_shortname'] = $this->input->post('bank_shortname');

        $data['account_no'] = $this->input->post('account_no', true);
        // $data['blance'] = $this->input->post('blance');
        // echo '<pre>';
        // print_r($data);
       // exit();


       $this->Dist_Panel_Model->update_bank_info($data,$bank_id);
        // $sdata['message']='Your Bank Update Successfully Created';
        // $this->session->set_userdata($sdata);
          redirect('Dist_panel/view_bank');
    }

public function add_transection() {
        $data = array();
        $data['title'] = 'Transection';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['bank_info']=$this->Dist_Panel_Model->select_all_bank($dist_id);
        $data['admin_master'] = $this->load->view('dist/bank/add_transection', $data, true);
        
        $this->load->view('dist/home', $data);
    }

    public function save_transection()
{     
$data = array();
        $bank_id =$this->input->post('bank_id', true);
        $type = $this->input->post('trans_type', true);
        $amount = $this->input->post('amount', true);

        $res = $this->Dist_Panel_Model->select_all_bank_only_one($bank_id);
        $result = $res->blance;

        if ($type == 'credit') {

            
            $data['blance'] = $result +$amount;
             $this->Dist_Panel_Model->update_bank_trans_info($data,$bank_id);
       //        echo '<pre>';
       // print_r($ammount);
       // exit();
        }
        else
        {
             $data['blance'] = $result - $amount;
             $this->Dist_Panel_Model->update_bank_trans_info($data,$bank_id);
        }


       $this->Dist_Panel_Model->save_trans_info();
        redirect('Dist_panel/view_transection');
}

public function view_bank() {
        
        $data = array();
        $data['title'] = 'View Bank';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['bank_info'] = $this->Dist_Panel_Model->select_all_bank($dist_id);
        $data['admin_master'] = $this->load->view('dist/bank/view_bank', $data, true);
        $this->load->view('dist/home', $data);
    
    }

    public function view_transection()
    {

        $data = array();
        $data['title'] = 'View Transection';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['trans_info'] = $this->Dist_Panel_Model->select_all_transbank($dist_id);
        $data['admin_master'] = $this->load->view('dist/bank/view_transection', $data, true);
        $this->load->view('dist/home', $data);
    }

    //// Company Controller


     public function new_emp() {
        $data = array();
        $data['title'] = 'Add Employee';
         $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['admin_master'] = $this->load->view('dist/employee/add_emp', $data, true);
        $this->load->view('dist/home', $data);
    }

    /// Save Dist Info
    

     public function save_emp() {

        $this->Dist_Panel_Model->save_emp_info();
        redirect('Dist_panel/new_emp');
    }




public function view_emp() {
        
        $data = array();
        $data['title'] = 'View Employee All';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['emp_info'] = $this->Dist_Panel_Model->select_emp_info_all($dist_id);

       //    echo '<pre>';
       // print_r($data);
       // exit();
        $data['admin_master'] = $this->load->view('dist/employee/view_all', $data, true);
        $this->load->view('dist/home', $data);
    
    }

public function edit_employee($emp_id) {
        $data = array();
        $data['title'] = 'Add Employee';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['emp_info'] = $this->Dist_Panel_Model->emp_info_by_id($emp_id);
        $data['admin_master'] = $this->load->view('dist/employee/edit_emp', $data, true);
        $this->load->view('dist/home', $data);
    }

public function update_emp()
    {
           $data = array();

 $emp_id = $this->input->post('emp_id', true);  
$data['emp_name'] = $this->input->post('emp_name', true);
$data['emp_phone'] = $this->input->post('emp_phone');


$data['emp_email'] = $this->input->post('emp_email');

$data['emp_address'] = $this->input->post('emp_address');


$data['emp_nid'] = $this->input->post('emp_nid');

$data['emp_degi'] = $this->input->post('emp_degi');

if ($_FILES['emp_picture']['name'] != NULL) {
            $config['upload_path'] = './files/emp/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('emp_picture')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Dist_panel/view_emp');
            } else {
                $fdata = $this->upload->data();
                $data['emp_picture'] = $config['upload_path'] . $fdata['file_name'];
            }
        }
$data['emp_salary'] = $this->input->post('emp_salary');


$dist_id = $this->session->userdata('dist_id');


  $data['dist_id'] = $dist_id;   
        



$dist_id =$this->session->userdata('dist_id');

   $this->Dist_Panel_Model->update_empl_info($data,$emp_id);
        redirect('Dist_panel/view_emp');  

    }



public function delete_employee($emp_id)
{
   $this->Dist_Panel_Model->delete_emp_info($emp_id);
        
        redirect('Dist_Panel/view_emp');
}






 //// Edit Profile

      public function password($dist_id) {
        $data = array();
         $data['title'] = 'Change Password';
         $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        //$dist_id = $this->session->userdata('dist_id');
        $data['dist_info'] = $this->Dist_Panel_Model->select_distributor_profile($dist_id);

       
        $data['admin_master'] = $this->load->view('dist/profile/chng_pass', $data, true);
        $this->load->view('dist/home', $data);
    }



public function change_password(){
        

 $data = array();
 $dist_id = $this->input->post('dist_id', true);

$admin_password1 = md5($this->input->post('admin_password1'));

$admin_password2 = md5($this->input->post('admin_password2'));
         
   if ($admin_password1 != $admin_password2 ) {
                $sdata['message']='Not Match !';
                 $this->session->set_userdata($sdata);
                redirect('Dist_panel/password/'.$dist_id);
   }
   else {


        $data['dist_password'] = md5($this->input->post('admin_password1'));

        $this->Dist_Panel_Model->update_dist_password($data, $dist_id);
        redirect('Dist_Panel/distributor_profile');   
        
       }


    }




    ///// Disributor Logout


      public function distributor_logout() {
        $this->session->unset_userdata('dist_id');
        $this->session->unset_userdata('dist_name');
        $sdata = array();
        $sdata['message'] = 'You Are Successfully Log-Out ';
        $this->session->set_userdata($sdata);
        redirect('Home');
    }








}

