<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ael_panel extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 public function __construct() {
        parent::__construct();

        $admin_id = $this->session->userdata('admin_id');
        
        if ($admin_id == NULL) {
            redirect('Ael_admin');
        }
    }





	public function index()
	{
		$data = array();
		$data['title'] = "Home | Ael";
        $data['admin_id'] = $this->session->userdata('admin_id');
		$data['admin_master'] = $this->load->view('admin/page_content', $data, true);
		$this->load->view('admin/home',$data);
	}

////// Distributor Controller Start 
	  /// Add Distributor Info

	    public function new_distributor() {
        $data = array();
        $data['title'] = 'Add Distributor';
        $data['admin_id'] = $this->session->userdata('admin_id');
        $data['admin_master'] = $this->load->view('admin/dist/add_dist', $data, true);
        $this->load->view('admin/home', $data);
    }

    /// Save Dist Info
	

     public function save_disributor() {
        $this->Ael_Panel_Model->save_dist_info();
        redirect('Ael_panel/new_distributor');
    }


    //// View All Distributor

      public function view_distributor() {
        
        $data = array();
        $data['title'] = 'View All Distributor';
         $data['admin_id'] = $this->session->userdata('admin_id');
        $data['dist_info'] = $this->Ael_Panel_Model->select_all_view_dist();
        $data['admin_master'] = $this->load->view('admin/dist/view_dist', $data, true);
        $this->load->view('admin/home', $data);
    

    }

    /// edit distributor


     public function edit_distributor($dist_id) {
        $data = array();
         $data['title'] = 'Edit Distributor';
        $data['admin_id'] = $this->session->userdata('admin_id');
        $data['dist_info'] = $this->Ael_Panel_Model->select_dist_info_by_id($dist_id);

       
        $data['admin_master'] = $this->load->view('admin/dist/edit_dist', $data, true);
        $this->load->view('admin/home', $data);
    }


    public function update_dist(){
        

 $data = array();
 $dist_id = $this->input->post('dist_id', true);
$data['dist_name'] = $this->input->post('dist_name', true);
$data['dist_email'] = $this->input->post('dist_email', true);
//$data['dist_password'] = md5($this->input->post('dist_password'));
$data['dist_phone'] = $this->input->post('dist_phone', true);
$data['dist_address'] = $this->input->post('dist_address');


         
   if ($_FILES['dist_picture']['name'] != NULL) {
            $config['upload_path'] = './files/dist/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('dist_picture')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Ael_panel/new_distributor');
            } else {
                $fdata = $this->upload->data();
                $data['dist_picture'] = $config['upload_path'] . $fdata['file_name'];
            }
        }

        $data['zone'] = $this->input->post('zone');

        $this->Ael_Panel_Model->update_dist_info($data, $dist_id);
        redirect('Ael_Panel/view_distributor');   
        
       


    }


    //// Delete Distributor

      public function delete_distributor($dist_id,$dist_picture) {

        $this->Ael_Panel_Model->delete_dist_info($dist_id,$dist_picture);
        
        redirect('Ael_Panel/view_distributor');
    }


    //// End Distributor Controller



    ////// User Profile Controller

    ///View User Profile


public function profile() {
        $data = array();
        $data['title'] = 'Add Distributor';
         $data['admin_id'] = $this->session->userdata('admin_id');
         $admin_id = $this->session->userdata('admin_id');
         $data['admin_info'] = $this->Ael_Panel_Model->select_admin_profile($admin_id);
        $data['admin_master'] = $this->load->view('admin/users/profile', $data, true);
        $this->load->view('admin/home', $data);
    }


    //// Edit Profile

      public function edit_profile($admin_id) {
        $data = array();
         $data['title'] = 'Edit Admin Profile';
       $data['admin_id'] = $this->session->userdata('admin_id');
        $data['admin_info'] = $this->Ael_Panel_Model->select_admin_profile($admin_id);

       
        $data['admin_master'] = $this->load->view('admin/users/edit_profile', $data, true);
        $this->load->view('admin/home', $data);
    }


 public function update_profile(){
        

 $data = array();
 $admin_id = $this->input->post('admin_id', true);
$data['admin_name'] = $this->input->post('admin_name', true);
$data['admin_email'] = $this->input->post('admin_email', true);
$data['admin_password'] = md5($this->input->post('admin_password'));
$data['admin_address'] = $this->input->post('admin_address');
$data['admin_phone'] = $this->input->post('admin_phone');

         
   if ($_FILES['admin_picture']['name'] != NULL) {
            $config['upload_path'] = './files/users/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = 2048;
            //$config['max_width']            = 1024;
            //$config['max_height']           = 768;
            $error = '';
            $fdata = array();
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('admin_picture')) {
                $error = $this->upload->display_errors();
                $sdata = array();
                $sdata['exception'] = $error;
                $this->session->set_userdata($sdata);
                redirect('Ael_panel/profile');
            } else {
                $fdata = $this->upload->data();
                $data['admin_picture'] = $config['upload_path'] . $fdata['file_name'];
            }
        }

       

        $this->Ael_Panel_Model->update_admin_info($data, $admin_id);
        redirect('Ael_Panel/profile');   
        
       


    }


    /// End Profile Users

////// Change apssword


     //// Edit Profile

      public function change_password($admin_id) {
        $data = array();
         $data['title'] = 'Change Admin Password';
        $data['admin_id'] = $this->session->userdata('admin_id');
        $data['admin_info'] = $this->Ael_Panel_Model->select_admin_profile($admin_id);

       
        $data['admin_master'] = $this->load->view('admin/users/chng_pass', $data, true);
        $this->load->view('admin/home', $data);
    }



public function update_password(){
        

 $data = array();
 $admin_id = $this->input->post('admin_id', true);

$admin_password1 = md5($this->input->post('admin_password1'));

$admin_password2 = md5($this->input->post('admin_password2'));
         
   if ($admin_password1 != $admin_password2 ) {
                $sdata['message']='Not Match !';
                 $this->session->set_userdata($sdata);
                redirect('Ael_panel/change_password/'.$admin_id);
   }
   else {


        $data['admin_password'] = md5($this->input->post('admin_password1'));

        $this->Ael_Panel_Model->update_admin_password($data, $admin_id);
        redirect('Ael_Panel/profile');   
        
       }


    }


    //// Messageing Controller 

    // Inbox Data


    public function inbox() {
       //$data['admin_id'] = $this->session->userdata('admin_id');
       $this->load->view('admin/message/inbox');
        //$this->load->view('admin/home', $data);
    }




















    ///// Admin Logout


      public function admin_logout() {
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('admin_name');
        $sdata = array();
        $sdata['message'] = 'You Are Successfully Log-Out ';
        $this->session->set_userdata($sdata);
        redirect('Ael_admin');
    }






}
