<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ael_admin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */



	   public function __construct() {
        parent::__construct();
        
        $admin_id=  $this->session->userdata('admin_id');
        // $branch_id = $this->session->set_userdata('branch_id');
      // $access_label = $this->session->userdata('access_label');
        if($admin_id!=NULL){
            redirect('Ael_panel');
        }
    }





	public function index()
	{
		$this->load->view('admin/login');
	}

	 public function admin_login(){
        
        $admin_email=  $this->input->post('admin_email',TRUE);
        $admin_password=  $this->input->post('admin_password',TRUE);
        $result=$this->Ael_Model->admin_login_info($admin_email,$admin_password);
        $sdata=array();
        if($result){
            $sdata['admin_id']=$result->admin_id;
            $sdata['admin_name']=$result->admin_name;
           
            //$sdata['dist_picture'] = $result->dist_picture;

            $this->session->set_userdata($sdata);
            redirect('Ael_panel');
        }  else {
            $sdata['message']='Your Email Or Password Invalid !';
            $this->session->set_userdata($sdata);
            redirect('Ael_admin');
        }
    }






}
