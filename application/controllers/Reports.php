<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends CI_Controller {

	 public function __construct() {
        parent::__construct();

        $dist_id = $this->session->userdata('dist_id');
       // $picture = $this->session->userdata('dist_pic');
        if ($dist_id == NULL) {
            redirect('Home');
        }
    }



    public function purchase_form() {
        $data = array();
        $data['title'] = 'Search Purchase Reports';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_view_comp($dist_id);

        $data['admin_master'] = $this->load->view('dist/reports/purchase_form', $data, true);
        $this->load->view('dist/home', $data);
    }





  public function purchase_report() {
        $data = array();
        $data['title'] = 'Purchase Reports';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        
        $start=$this->input->post('start');
        $end=$this->input->post('end');
        $id=$this->input->post('comp_id');

        $data['p_info'] = $this->Report_Model->view_purchase_reports($start,$end,$id);
       
        $data['c_info'] = $this->Report_Model->view_purchase_reports_com($id);
       
       //  echo '<pre>';
       // print_r( $data);
       // exit();
         $this->session->set_userdata($start);
         $this->session->set_userdata($end);

        $data['admin_master'] = $this->load->view('dist/reports/purchase_view', $data, true);
        $this->load->view('dist/home', $data);
    }









}