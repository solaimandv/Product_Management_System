
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	 public function __construct() {
        parent::__construct();

        $dist_id = $this->session->userdata('dist_id');
        
        if ($dist_id == NULL) {
            redirect('Home');
        }
    }






public function purchase_product($product_id) {
        $data = array();
        $data['title'] = 'Purchases Product';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        $data['cat_info'] = $this->Dist_Panel_Model->select_all_Pcat($dist_id);
        $data['comp_info'] = $this->Dist_Panel_Model->select_all_company($dist_id);
        $data['product_info'] = $this->Dist_Panel_Model->select_product_by_id($product_id);
        $data['admin_master'] = $this->load->view('dist/product/purchase_product', $data, true);
        $this->load->view('dist/home', $data);
    }

public function purchase_invoice() {
        $data = array();
        $data['title'] = 'Purchases Product';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
       
        $data['purchase_info'] = $this->Product_Model->select_purchase_invoice($dist_id);
         $data['admin_master'] = $this->load->view('dist/purchase/invoice', $data, true);
        $this->load->view('dist/home', $data);
    }


public function update_product($product_id) {
    
 
$stock = $this->Product_Model->check_stock_table($product_id);

//$check = $this->db->query("SELECT * FROM tbl_stock Where product_id = $product_id");
// $checkrow = $stock->row();
// $ID = $checkrow->product_id;



   if (!empty($stock->product_id)) {



     $data = array();
     $data['barcode'] = $this->input->post('barcode', true);   
     $qty1 = $this->input->post('product_qty', true); 
     $qty2 = $stock->product_qty;
     $data['product_qty'] = $qty1 + $qty2;  

   $this->Product_Model->update_product_stocks_info($data,$product_id);
   $this->Product_Model->insert_product_purchase_innew();

      redirect('Product/purchase_invoice/'); 


       // $sdata = array();
       // $barcode = $this->input->post('barcode', true);
       // $qty = $this->input->post('product_qty', true); 
       
       // $barcodes[0]=$barcode;
       // $sdata['barcode']=$barcodes;
       // $sdata['ID']=$stock->product_id;
       // $this->load->view('dist/product/barcode', $sdata);

   //redirect('Product/purchase_product/'.$ID); 
     	 
     } 

     else 
     {

        $data = array();
        
$data['product_id'] = $this->input->post('product_id');
$data['invoice'] = $this->input->post('invoice');
$data['barcode'] = $this->input->post('barcode');

$data['product_name'] = $this->input->post('product_name');
$data['product_details'] = $this->input->post('product_details');
$data['product_qty'] = $this->input->post('product_qty');

$data['purchase_price'] = $this->input->post('purchase_price');
$data['retails_price'] = $this->input->post('retails_price');
$data['sale_price'] = $this->input->post('sale_price');

$data['tax_vat'] = $this->input->post('tax_vat');

$dist_id = $this->session->userdata('dist_id');

$data['dist_id'] = $dist_id;  


$data['comp_id'] = $this->input->post('comp_id');


 $this->Product_Model->insert_product_stocks_info($data);
$this->Product_Model->insert_product_purchase_innew();

       // $sdata = array();
       // $barcode = $this->input->post('barcode', true);
       // $qty = $this->input->post('product_qty', true); 
       // $IDS = $this->input->post('product_id', true);

       // $barcodes[0]=$barcode;
       // $sdata['barcode']=$barcodes;
       // $sdata['ID']=$IDS;
       // $this->load->view('dist/product/barcode', $sdata);

redirect('Product/purchase_invoice/'); 


     }
   }




public function product_purchase() {
        $data = array();
        $data['title'] = 'Purchases All';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
        // $data['cat_info'] = $this->Dist_Panel_Model->select_all_Pcat();
        // $data['comp_info'] = $this->Dist_Panel_Model->select_all_company($dist_id);
        $data['comp_info'] = $this->Product_Model->select_product_purchases_comp($dist_id);
        $data['admin_master'] = $this->load->view('dist/product/all_purchase', $data, true);
        $this->load->view('dist/home', $data);
    }




public function supplier_quantity($comp_id) {
        $data = array();
        $data['title'] = 'Purchases All Quantity';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
       
        $data['comp_info'] = $this->Product_Model->select_product_purchases_comp_qty($comp_id);

        $data['name'] = $this->Product_Model->select_product_name_comp($comp_id);

       // echo '<pre>';
       // print_r($data['comp_info']);
       // exit();


        $data['admin_master'] = $this->load->view('dist/product/supplier_quantity', $data, true);
        $this->load->view('dist/home', $data);
    }


public function all_barcode($comp_id) {
        $data = array();
        $data['title'] = 'Barcode All Quantity';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');
       
       

        $data['name'] = $this->Product_Model->select_product_name_comp($comp_id);

      
       $barcodes[0]=$barcode;
       $sdata['barcode']=$barcodes;
       $sdata['ID']=$stock->product_id;
       $this->load->view('dist/product/barcode', $sdata);

       // echo '<pre>';
       // print_r($data['comp_info']);
       // exit();


        $data['admin_master'] = $this->load->view('dist/product/supplier_quantity', $data, true);
        $this->load->view('dist/home', $data);
    }





 public function purchase_all() {
        
        $data = array();
        $data['title'] = 'View Product';
        $data['dist_id'] = $this->session->userdata('dist_id');
        $data['dist_name'] = $this->session->userdata('dist_name');
        $dist_id = $this->session->userdata('dist_id');
        $data['product_info'] = $this->Dist_Panel_Model->select_all_product($dist_id);
        $data['admin_master'] = $this->load->view('dist/purchase/all_purchase', $data, true);
        $this->load->view('dist/home', $data);
    
    }
 

  



















}