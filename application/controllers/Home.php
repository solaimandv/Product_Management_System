<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */




	   public function __construct() {
        parent::__construct();
        
        $dist_id=  $this->session->userdata('dist_id');
       // $picture = $this->session->set_userdata('dist_picture');
      
        if($dist_id!=NULL){
            redirect('Dist_panel');
        }
    }






	public function index()
	{
		$this->load->view('login');
	}



public function distributor_login(){
        
        $dist_email=  $this->input->post('dist_email',TRUE);
        $dist_password=  $this->input->post('dist_password',TRUE);
        $result=$this->Dist_Model->dist_login_info($dist_email,$dist_password);
        $sdata=array();
        if($result){
            $sdata['dist_id']=$result->dist_id;
            $sdata['dist_name']=$result->dist_name;
            $dpic = $result->dist_picture;
            $dist_pic= $this->session->set_userdata($dpic);
       //      echo '<pre>';
       // print_r( $result->dist_picture);
       // exit();
            $this->session->set_userdata($sdata);
            redirect('dist_panel');
        }  else {
            $sdata['message']='Your Email Or Password Invalid !';
            $this->session->set_userdata($sdata);
            redirect('Home');
        }
    }


	






}
