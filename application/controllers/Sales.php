<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {

	 public function __construct() {
        parent::__construct();

        $dist_id = $this->session->userdata('dist_id');
        
        if ($dist_id == NULL) {
            redirect('Home');
        }
    }




 public function index() {
         $data = array();
        $t = time();
        $data['title'] = 'Sale Product';
        $data['dist_name'] = $this->session->userdata('dist_name');
        $data['dist_id'] = $this->session->userdata('dist_id');
        $dist_id = $this->session->userdata('dist_id');

        $data['product_info'] = $this->Dist_Panel_Model->select_all_product($dist_id);
        $data['invoice'] = $t . "-" . $dist_id;
         $query = $this->db->query("select * from tbl_distributor where dist_id = $dist_id");
        $result = $query->result();

     $this->load->view('dist/sales/product_sales', $data);
    //$this->load->view('dist/home', $data);

   }


    function addtocart() {
        $barcode = $this->input->post('barcode', true);
        $dist_id = $this->session->userdata('dist_id');

      // print_r($dist_id);
      //  die();

        $qtyneed = $this->input->post('qtyneed', true);;
      
        $query = $this->db->query("select * from tbl_stock where barcode = '$barcode' and dist_id = '$dist_id'");

        $result = $query->result();


      // print_r($result);
      //  die();


        if (isset($result[0])) {

            
                $rp = $result[0]->retails_price;
                $pp = $result[0]->purchase_price;
                $product_qty = $result[0]->product_qty;

       // print_r($rp);
       // die();

                $product_qty_array = json_decode($product_qty, true);
                // print_r($product_qty_array);

                if (isset($product_qty_array)) {

                    if ($product_qty_array > 0) {
                        /* quantity is available start */
                        
                        
                       


                       
                        $t = $rp * $qtyneed;
                        $profit = $rp - $pp;
                        echo '<tr class="' . $result[0]->barcode . '" id="' . $result[0]->stock_id . '">
                         <input type="hidden" value="' . $result[0]->comp_id . '" name="comp_id"  />
                              <input type="hidden" value="' . $result[0]->stock_id . '" name="stock_id[]"  />
                              <input type="hidden" value="' . $result[0]->barcode . '" name="barcode_num[]"  />
                              <input type="hidden" value="' . $profit . '" name="profit[]"  />
                              <input type="hidden" value="' . $result[0]->product_name . '-(' . $result[0]->barcode . ')" name="product_name[]"  />
                              <input type="hidden" value="' . $result[0]->retails_price . '" name="retails_price[]"  />
                              <input type="hidden" value="' . $qtyneed . '" name="quantity[]"  />
                              <td width="40%">' . $result[0]->product_name . '-(' . $result[0]->barcode . ')</td>
                              <td width="10%" >' . $result[0]->retails_price . '</td>
                              <td width="10%">' . $qtyneed .'-(' . $result[0]->product_qty . ') </td>

                              <td width="15%">'. $result[0]->tax_vat.'</td>

                              <td width="10%">-</td>

                              <td width="20%"><a id="totalprice">' . $t . '</a><i onclick="remove_product(' . $result[0]->stock_id . ')" style="float:right;cursor:pointer;" class="fa fa-remove"></i>

                              </td>

                              </tr>';
                       
                        /* quantity is available end */
              
                }

                else {
                	echo"2";
                }

	                }

	                else {
	                	echo "2";
	                }

	            }

	            else {

	            	echo "3";
	            }
           
  
}




    function sales() {
        
        $session_data = $this->session->userdata('logged_in');
        $branch_id = $session_data['branch_id'];
        
        parse_str($_POST['sales'], $salesarray);
        $bar = $barcode_id = $salesarray['barcode_id'];

        $session_data = $this->session->userdata('logged_in');
        $salesarray['sales_by'] = $session_data['username'];
        $salesarray['user_id'] = $session_data['id'];

        $salesarray['barcode_id'] = json_encode($barcode_id);

          $salesarray['supplier'] = $salesarray['supplier'];

        $barcode = $salesarray['barcode_num'];
        $salesarray['barcode_num'] = json_encode($barcode);

        $quantity = $salesarray['quantity'];
        $qaty=$salesarray['quantity'] = json_encode($quantity);
       
        $product_name = $salesarray['product_name'];
        $salesarray['product_name'] = json_encode($product_name);

        $retails_price = $salesarray['retails_price'];
        $salesarray['retails_price'] = json_encode($retails_price);


        $profits = $salesarray['profit'];
        /*
          $tprofit = 0;
          foreach ($profits as $profit) {
          $tprofit = $tprofit + $profit;
          }
         */
         
        /*Set The Bangladeshi Time Zone*/
        date_default_timezone_set('Asia/Dhaka');
        // Then call the date functions
        $current_time = date('d-m-Y h:i:sa');
        $salesarray['sale_date_time'] = $current_time;
        
        $current_sales_date = date('Y-m-d h:i:s');
        $salesarray['current_sales_date'] = $current_sales_date;

        //echo $current_sales_date;exit;

        

        $salesarray['profit'] = json_encode($profits);

	 //var_dump($salesarray);exit;

        $this->db->insert('sales', $salesarray);
        $insert_id = $this->db->insert_id();
        $query1 = $this->db->query("select * from sales where (`sales_id` = '$insert_id')");
        $result1 = $query1->result();
        echo $result1[0]->invoice_id;

        
        $qty=$quantity;
        $q=0;
        foreach ($bar as $code) {
            $update = array();
            $query = $this->db->query("select * from barcode where barcode_id = $code");
            $res = $query->result();
            $branch_quantity = $res[0]->branch_quantity;
            $branch_qty_array = json_decode($branch_quantity, true);
            $branch_qty_array[$branch_id] = $branch_qty_array[$branch_id] - $qty[$q];
            $update['branch_quantity'] = json_encode($branch_qty_array);
            $this->db->where('barcode_id', $code);
            $this->db->update('barcode', $update);
           
            $qty[$q];
            $q++;
            
        }        
       
        /*
        foreach ($bar as $id) {
            $update = array(
                'sales_status' => 1
            );
            $this->db->where('barcode_id', $id);
            $this->db->update('barcode', $update);
        }
         */

        $total = $salesarray['cash'];
        $this->db->query("UPDATE branch set branch_account = branch_account + '$total' WHERE branch_id = '$branch_id'");

    }



  


}


