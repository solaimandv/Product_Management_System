-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2017 at 02:00 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ael`
--

-- --------------------------------------------------------

--
-- Table structure for table `supplier_payment`
--

CREATE TABLE `supplier_payment` (
  `spay_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `s_amount` varchar(50) NOT NULL,
  `dist_ id` int(11) NOT NULL,
  `dist_name` varchar(50) NOT NULL,
  `spay_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `admin_id` int(9) NOT NULL,
  `admin_name` varchar(100) NOT NULL,
  `admin_email` varchar(100) NOT NULL,
  `admin_password` varchar(120) NOT NULL,
  `admin_address` varchar(250) NOT NULL,
  `admin_phone` varchar(20) NOT NULL,
  `admin_picture` varchar(130) NOT NULL,
  `join_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `admin_label` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_address`, `admin_phone`, `admin_picture`, `join_date`, `admin_label`) VALUES
(1, 'AEL', 'info@ael.com', '21232f297a57a5a743894a0e4a801fc3', '<p>Kawran Bazar, Dhaka-1200,Dhaka</p>\r\n', '017**********', './files/users/User-icon.png', '2017-07-12 22:35:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cat_id` int(9) NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_details` varchar(250) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cat_id`, `cat_name`, `cat_details`, `dist_id`, `comp_id`) VALUES
(1, 'asd', 'zv zzxcv', 7, 1),
(2, '12 KG', '<p>sdfgh</p>\r\n', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE `tbl_company` (
  `comp_id` int(11) NOT NULL,
  `comp_name` varchar(200) NOT NULL,
  `comp_add` text NOT NULL,
  `comp_logo` varchar(150) NOT NULL,
  `comp_color` varchar(150) NOT NULL,
  `comp_phone` varchar(50) NOT NULL,
  `comp_email` varchar(100) NOT NULL,
  `dist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`comp_id`, `comp_name`, `comp_add`, `comp_logo`, `comp_color`, `comp_phone`, `comp_email`, `dist_id`) VALUES
(1, 'Beximco Gas', '<p>Mirpur DOHS, Mirpur-12, Dhaka</p>\r\n', './files/comp/cmp.png', 'red', '017&&&&&&', 'comp1@gmail.com', 0),
(2, 'Basundhara Gas', '<p>Mirpur-10, Dhaka-1216, Dhaka</p>\r\n', './files/comp/cmp1.png', 'red', '017&&&&&&', 'comp1@gmail.com', 7),
(3, 'Laugh Gas', '<p>Mirpur-1, Dhaka</p>\r\n', './files/comp/cmp2.png', '#fff', '017&&&&&&', 'comp1@gmail.com', 7),
(4, 'Jamuna', '<p>Dhaka-1200, Dhaka</p>\r\n', './files/comp/cmp1.png', '#000', '019882737', 'bm@gmail.com', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_distributor`
--

CREATE TABLE `tbl_distributor` (
  `dist_id` int(9) NOT NULL,
  `dist_name` varchar(100) NOT NULL,
  `dist_email` varchar(100) NOT NULL,
  `dist_password` varchar(100) NOT NULL,
  `dist_phone` varchar(50) NOT NULL,
  `dist_address` text NOT NULL,
  `dist_label` tinyint(1) NOT NULL,
  `dist_picture` varchar(120) NOT NULL,
  `zone` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_distributor`
--

INSERT INTO `tbl_distributor` (`dist_id`, `dist_name`, `dist_email`, `dist_password`, `dist_phone`, `dist_address`, `dist_label`, `dist_picture`, `zone`) VALUES
(7, 'Distributor', 'dist@gmail.com', '2a6d07eef8b10b84129b42424ed99327', '0173********', 'Mirpur-10, Dhaka', 1, './files/dist/Juice_Pack_psd.png', ''),
(8, 'jamuna', 'jamuna@gmail.com', '4c7c5c1d7def04f85d7563faf02a6964', '01685695989', '<p>Dhaka</p>\r\n', 0, './files/dist/Juice_Pack_psd.png', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employee`
--

CREATE TABLE `tbl_employee` (
  `emp_id` int(11) NOT NULL,
  `emp_name` varchar(50) NOT NULL,
  `emp_phone` varchar(20) NOT NULL,
  `emp_email` varchar(120) NOT NULL,
  `emp_address` text NOT NULL,
  `emp_nid` varchar(22) NOT NULL,
  `emp_degi` varchar(35) NOT NULL,
  `emp_picture` varchar(120) NOT NULL,
  `emp_salary` varchar(50) NOT NULL,
  `dist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_employee`
--

INSERT INTO `tbl_employee` (`emp_id`, `emp_name`, `emp_phone`, `emp_email`, `emp_address`, `emp_nid`, `emp_degi`, `emp_picture`, `emp_salary`, `dist_id`) VALUES
(1, 'Mamun', '01712******8', 'mamun@gmail.com', '<p>Dhaka</p>\r\n', '123456789', 'co-ordinator', './files/emp/emp1.png', '20000', 7),
(2, 'Saleh Ahmed', '0199999999', 'saleh@gmail.com', '<p>Chadpur, Bangladesh</p>\r\n', '197689636917279', 'Manager', './files/emp/emp.png', '12500', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_expense`
--

CREATE TABLE `tbl_expense` (
  `expense_id` int(11) NOT NULL,
  `expense_type` varchar(20) NOT NULL,
  `expense_name` varchar(65) NOT NULL,
  `expense_cat` int(11) NOT NULL,
  `expense_note` varchar(100) NOT NULL,
  `expense_cost` varchar(20) NOT NULL,
  `expense_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_expense`
--

INSERT INTO `tbl_expense` (`expense_id`, `expense_type`, `expense_name`, `expense_cat`, `expense_note`, `expense_cost`, `expense_date`, `dist_id`, `comp_id`) VALUES
(1, 'BillElectricity', 'BillElectricity', 0, 'Hleeell', '1200', '2017-07-11 18:00:00', 7, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_product`
--

CREATE TABLE `tbl_product` (
  `product_id` int(9) NOT NULL,
  `cat_id` int(9) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `product_details` varchar(500) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `purchase_price` varchar(50) NOT NULL,
  `sale_price` varchar(50) NOT NULL,
  `retails_price` varchar(50) NOT NULL,
  `tax_vat` varchar(50) NOT NULL,
  `product_qty` varchar(50) NOT NULL,
  `product_pic` varchar(120) NOT NULL,
  `product_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_product`
--

INSERT INTO `tbl_product` (`product_id`, `cat_id`, `product_name`, `product_details`, `supplier_id`, `purchase_price`, `sale_price`, `retails_price`, `tax_vat`, `product_qty`, `product_pic`, `product_date`, `dist_id`, `comp_id`) VALUES
(1, 1, 'abc', 'abc', 1, '500', '500', '500', '10', '5', 'asdfghjkjhgfd', '2017-07-02 18:00:00', 7, 2),
(3, 1, 'tv', '<p>dfghj</p>\r\n', 0, '5545', '666', '46464', '446', '655', '', '2017-07-13 06:52:39', 7, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_purchase`
--

CREATE TABLE `tbl_purchase` (
  `purchase_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `invoice` varchar(32) NOT NULL,
  `purchase_price` varchar(50) NOT NULL,
  `sales_price` varchar(50) NOT NULL,
  `retail_price` varchar(50) NOT NULL,
  `qty` varchar(25) NOT NULL,
  `purchase_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_purchase`
--

INSERT INTO `tbl_purchase` (`purchase_id`, `product_id`, `invoice`, `purchase_price`, `sales_price`, `retail_price`, `qty`, `purchase_date`, `dist_id`, `comp_id`) VALUES
(1, 1, '11170717', '500', '500', '500', '2', '2017-07-17 08:52:38', 7, 2),
(2, 1, '11170717', '500', '500', '500', '2', '2017-07-17 08:53:00', 7, 2),
(3, 3, '13170717', '5545', '666', '46464', '5', '2017-07-17 08:54:18', 7, 4),
(4, 1, '11170717', '500', '500', '500', '2', '2017-07-17 09:05:51', 7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_salary`
--

CREATE TABLE `tbl_salary` (
  `salary_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `salary_amount` varchar(20) NOT NULL,
  `salary_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_salary`
--

INSERT INTO `tbl_salary` (`salary_id`, `emp_id`, `salary_amount`, `salary_date`, `dist_id`) VALUES
(1, 1, '3463777', '2017-07-18 18:00:00', 7);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sales`
--

CREATE TABLE `tbl_sales` (
  `sales_id` int(11) NOT NULL,
  `invoice` varchar(35) NOT NULL,
  `stock_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `barcode` varchar(22) NOT NULL,
  `product_name` varchar(65) NOT NULL,
  `purchase_price` varchar(22) NOT NULL,
  `retails_price` varchar(22) NOT NULL,
  `sale_price` varchar(22) NOT NULL,
  `tax_vat` varchar(15) NOT NULL,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stock`
--

CREATE TABLE `tbl_stock` (
  `stock_id` int(11) NOT NULL,
  `product_id` varchar(32) NOT NULL,
  `invoice` int(11) NOT NULL,
  `barcode` varchar(50) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `product_details` text NOT NULL,
  `product_qty` varchar(50) NOT NULL,
  `purchase_price` int(50) NOT NULL,
  `retails_price` varchar(50) NOT NULL,
  `sale_price` varchar(50) NOT NULL,
  `tax_vat` varchar(50) NOT NULL,
  `stock_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_stock`
--

INSERT INTO `tbl_stock` (`stock_id`, `product_id`, `invoice`, `barcode`, `product_name`, `product_details`, `product_qty`, `purchase_price`, `retails_price`, `sale_price`, `tax_vat`, `stock_date`, `dist_id`, `comp_id`) VALUES
(28, '1', 11170717, '1', 'abc', '<p>abc</p>\r\n', '10', 500, '500', '500', '3', '2017-07-17 08:39:11', 7, 2),
(29, '3', 13170717, '3-0', 'tv', '<p>dfghj</p>\r\n', '5', 5545, '46464', '666', '4', '2017-07-17 08:54:18', 7, 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_supplier`
--

CREATE TABLE `tbl_supplier` (
  `supplier_id` int(11) NOT NULL,
  `supplier_name` varchar(65) NOT NULL,
  `supplier_email` varchar(120) NOT NULL,
  `supplier_phone` varchar(20) NOT NULL,
  `supplier_address` varchar(250) NOT NULL,
  `supplier_detials` text NOT NULL,
  `supplier_amount` varchar(15) NOT NULL,
  `supplier_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `dist_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_supplier`
--

INSERT INTO `tbl_supplier` (`supplier_id`, `supplier_name`, `supplier_email`, `supplier_phone`, `supplier_address`, `supplier_detials`, `supplier_amount`, `supplier_date`, `dist_id`, `comp_id`) VALUES
(1, 'Sumon sarkar', 'sumon@gmail.com', '01712***********', 'Dhaka', 'Rangpur,', '34565', '2017-07-12 10:27:16', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(11) NOT NULL,
  `user_name` varchar(120) NOT NULL,
  `user_email` varchar(120) NOT NULL,
  `user_password` varchar(120) NOT NULL,
  `user_phone` varchar(18) NOT NULL,
  `user_address` varchar(250) NOT NULL,
  `user_label` tinyint(1) NOT NULL,
  `dist_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `user_name`, `user_email`, `user_password`, `user_phone`, `user_address`, `user_label`, `dist_id`) VALUES
(1, 'Hasan', 'hasan@gmail.com', '12345', '019282882', '<p>Mirpur-14</p>\r\n', 1, 7),
(2, 'Jubayer', 'jubayer@gmail.com', '12345', '019828228', '<p>Mirpur-1</p>\r\n', 2, 7),
(3, 'solaiman', 'solaiman.cse14@gmail.com', '123456', '01685695989', '<p>Tangail</p>\r\n', 1, 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `supplier_payment`
--
ALTER TABLE `supplier_payment`
  ADD PRIMARY KEY (`spay_id`);

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `tbl_distributor`
--
ALTER TABLE `tbl_distributor`
  ADD PRIMARY KEY (`dist_id`);

--
-- Indexes for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  ADD PRIMARY KEY (`emp_id`);

--
-- Indexes for table `tbl_expense`
--
ALTER TABLE `tbl_expense`
  ADD PRIMARY KEY (`expense_id`);

--
-- Indexes for table `tbl_product`
--
ALTER TABLE `tbl_product`
  ADD PRIMARY KEY (`product_id`);

--
-- Indexes for table `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  ADD PRIMARY KEY (`purchase_id`);

--
-- Indexes for table `tbl_salary`
--
ALTER TABLE `tbl_salary`
  ADD PRIMARY KEY (`salary_id`);

--
-- Indexes for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  ADD PRIMARY KEY (`sales_id`);

--
-- Indexes for table `tbl_stock`
--
ALTER TABLE `tbl_stock`
  ADD PRIMARY KEY (`stock_id`);

--
-- Indexes for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  ADD PRIMARY KEY (`supplier_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `supplier_payment`
--
ALTER TABLE `supplier_payment`
  MODIFY `spay_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `admin_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cat_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_distributor`
--
ALTER TABLE `tbl_distributor`
  MODIFY `dist_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tbl_employee`
--
ALTER TABLE `tbl_employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_expense`
--
ALTER TABLE `tbl_expense`
  MODIFY `expense_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_product`
--
ALTER TABLE `tbl_product`
  MODIFY `product_id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_purchase`
--
ALTER TABLE `tbl_purchase`
  MODIFY `purchase_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_salary`
--
ALTER TABLE `tbl_salary`
  MODIFY `salary_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_sales`
--
ALTER TABLE `tbl_sales`
  MODIFY `sales_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_stock`
--
ALTER TABLE `tbl_stock`
  MODIFY `stock_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tbl_supplier`
--
ALTER TABLE `tbl_supplier`
  MODIFY `supplier_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
